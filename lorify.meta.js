// ==UserScript==
// @name        lorify
// @namespace   https://bitbucket.org/b0r3d0m/lorify-userscript
// @description lorify provides you an autorefresh for threads and an easy way to view referenced comments on linux.org.ru's forums
// @include     /^https?:\/\/www.linux.org.ru\/forum\/.*$/
// @include     /^https?:\/\/www.linux.org.ru\/gallery\/.*$/
// @include     /^https?:\/\/www.linux.org.ru\/news\/.*$/
// @version     1.11.0
// @grant       none
// @require     http://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js
// @icon        http://icons.iconarchive.com/icons/icons8/christmas-flat-color/32/penguin-icon.png
// @updateURL   https://bitbucket.org/smaximov/lorify-userscript/raw/master/lorify.meta.js
// @downloadURL https://bitbucket.org/smaximov/lorify-userscript/raw/master/lorify.user.js
// ==/UserScript==
