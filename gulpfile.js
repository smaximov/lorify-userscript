/* eslint-env node */
'use strict';

const gulp = require('gulp');
const gutil = require('gulp-util');
const babel = require('gulp-babel');
const webpack = require('webpack-stream');
const concat = require('gulp-concat');
const template = require('gulp-template');
const mocha = require('gulp-mocha');
const newer = require('gulp-newer');
const fs = require('fs');
const ini = require('ini');
const esdoc = require('gulp-esdoc');

const pkg = require('./package.json');
const esdocConfig = require('./esdoc.json');

gulp.task('build', () => {
  const dest = 'build';
  return gulp.src(['src/**/*.js', '!src/lorify.meta.js'])
    .pipe(newer(dest))
    .pipe(babel())
    .pipe(gulp.dest(dest));
});

gulp.task('pack', ['build'], () => {
  const dest = 'build';
  const webpackConfig = {
    output: {
      filename: 'lorify.user.js'
    }
  };
  return gulp.src('build/index.js')
    .pipe(webpack(webpackConfig))
    .pipe(gulp.dest(dest));
});

gulp.task('meta', () => {
  const bindings = {
    version: pkg.version
  };
  return gulp.src('src/lorify.meta.js')
    .pipe(template(bindings))
    .pipe(gulp.dest('.'));
});

gulp.task('concat', ['meta', 'pack'], () => {
  return gulp.src(['lorify.meta.js', 'build/lorify.user.js'])
    .pipe(concat('lorify.user.js'))
    .pipe(gulp.dest('.'));
});

gulp.task('test', ['build'], () => {
  const mochaConfig = {
    require: ['./test/helper']
  };
  return gulp.src('test/**/test_*.js')
    .pipe(mocha(mochaConfig))
});

// Return `Default` profile or the first profile, if any
function getDefaultProfile(profiles) {
  let result = null;
  for (let key in profiles) {
    if (/^Profile\d+$/.test(key)) {
      const profile = profiles[key];

      if (profile.Default) {
        result = profile;
        break;
      }
      if (result === null) {
        result = profile;
      }
    }
  }
  return result;
}

gulp.task('docs', () => {
  return gulp.src("./src")
    .pipe(esdoc(esdocConfig));
});

gulp.task('install:firefox', ['concat', 'test'], () => {
  const firefoxDirectory = `${process.env.HOME}/.mozilla/firefox`;
  const profilesPath = `${firefoxDirectory}/profiles.ini`;
  const profiles = ini.parse(fs.readFileSync(profilesPath, 'utf-8'));
  const profile = getDefaultProfile(profiles);

  if (profile === null) {
    gutil.log(gutil.colors.red(`Cannot fild default firefox profile in ${profilesPath}`));
    process.exit(1);
  }

  gutil.log(gutil.colors.green('Found'), `firefox profile '${profile.Path}'`);

  const userScriptDir = `${firefoxDirectory}/${profile.Path}/gm_scripts/lorify`;
  const userScriptPath = `${userScriptDir}/lorify.user.js`;

  const stat = fs.statSync(userScriptPath)
  if (!stat.isFile()) {
    gutil.log(gutil.colors.red('No previous version of the script was found'));
    gutil.log(gutil.colors.red(`Looked for '${userScriptPath}'`));
    process.exit(1);
  }

  gutil.log(gutil.colors.green('Found'), 'previous version of the script');

  return gulp.src('lorify.user.js')
    .pipe(gulp.dest(userScriptDir));
});

gulp.task('default', ['concat']);
