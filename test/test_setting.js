/* eslint-env mocha, node */
/* global expect */
'use strict';

const setting = require('../build/setting');
const Storage = require('../build/storage').Storage;
const Setting = setting.Setting;
const IntSetting = setting.IntSetting;
const BoolSetting = setting.BoolSetting;

function MockStorage() {
  Storage.call(this);
  this.storage = {};
}

MockStorage.prototype = Object.create(Storage.prototype);
MockStorage.prototype.constructor = MockStorage;

MockStorage.prototype.store = function(setting) {
  this.storage[setting.key] = setting.value;
}

MockStorage.prototype.load = function(setting) {
  return this.storage[setting.key];
}

describe('Setting', () => {
  const storage = new MockStorage();

  it('should return initial value', () => {
    const value = 'string value';
    const setting = new Setting(storage, 'initial-value', value);

    expect(setting.value).to.be.equal(value);
  });

  it('should be persistent by default', () => {
    const setting = new Setting(storage, 'persistent-default', null);

    expect(setting.persistent).to.be.true;
  })

  it('should store values as strings', () => {
    const value = 'string value';
    const empty = '';
    const setting = new Setting(storage, 'values-as-strings', null);

    setting.value = value;
    expect(setting.value).to.be.equal(value);

    setting.value = empty;
    expect(setting.value).to.be.equal(empty);
  });

  describe('validate', () => {
    context('without validators', () => {
      it('should accept any value', () => {
        const setting = new Setting(storage, 'no-validators', null);

        expect(setting.validate(process.env)).to.be.true;
        expect(setting.validate(Math.random())).to.be.true;
      });
    });

    context('with Number.isInteger validator', () => {
      it('should accept integers', () => {
        const setting = new Setting(storage, 'with-validator-accept-integers', null);
        setting.validator(Number.isInteger);

        expect(setting.validate(2)).to.be.true;
        expect(setting.validate(-2)).to.be.true;
      });

      it('should reject non-integers', () => {
        const setting = new Setting(storage, 'with-validator-reject-non-integers', null);
        setting.validator(Number.isInteger);

        expect(setting.validate(2.2)).to.be.false;
        expect(setting.validate('')).to.be.false;
      });
    });
  });

  describe('storage', () => {
    context('persistent', () => {
      it('should be able to load previously stored value', () => {
        const value = 'foo';
        const newValue = 'new-value';
        const setting = new Setting(storage, 'load-stored', value);

        setting.save();
        setting.value = newValue;

        expect(setting.value).to.be.equal(newValue);

        setting.load();

        expect(setting.value).to.be.equal(value);
      });
    })

    context('transient', () => {
      it('should ignore storing and loading', () => {
        const value = 'foo';
        const newValue = 'bar';
        const setting = new Setting(storage, 'ignore-load-stored', value, {
          persistent: false
        });

        setting.save();
        setting.value = newValue;

        expect(setting.value).to.be.equal(newValue);

        setting.load();
        expect(setting.value).to.be.equal(newValue);
      });
    });
  });

  describe('change', () => {
    context('with a change hook', () => {
      it('should run the hook after the change', (done) => {
        const value = 'old-value';
        const newValue = 'new-value';
        const setting = new Setting(storage, 'change', value);

        setting.onChange((changedValue, oldVal, target) => {
          expect(target.value).to.be.equal(changedValue);
          expect(target.value).to.be.equal(newValue);
          expect(target).to.be.deep.equal(setting);
          expect(oldVal).to.be.equal(value);
          done();
        });

        setting.change(newValue);
      });

      it('should not run the hook if the new value is the same as the old one', () => {
        const value = 'old-value';
        const setting = new Setting(storage, 'not-change', value);

        setting.onChange(() => {
          throw new Error('the hook should be unreachable');
        })

        setting.change(value);
      });
    });
  });

  describe('parse', () => {
    context('without parser', () => {
      it('should return the argument', () => {
        const value = 'some string';
        const empty = '';
        const setting = new Setting(storage, 'without-parser', null);

        expect(setting.parse(value)).to.be.equal(value);
        expect(setting.parse(empty)).to.be.equal(empty);
      });
    });

    context('with JSON parser', () => {
      it('should parse JSON', () => {
        const json = '{ "key": "value" }';
        const value = { "key": "value" };
        const setting = new Setting(storage, 'with-json-parser', null);
        setting.parser(JSON.parse);

        expect(setting.parse(json)).to.be.deep.equal(value);
      });
    });
  });
});

describe('IntSetting', () => {
  const storage = new MockStorage();

  it('should have default value 0', () => {
    expect(new IntSetting(storage, 'default').value).to.be.equal(0);
  });

  describe('type validation', () => {
    it('should accept integers', () => {
      const setting = new IntSetting(storage, 'accept-integers');

      expect(setting.validate(0)).to.be.true;
      expect(setting.validate(1)).to.be.true;
      expect(setting.validate(-1)).to.be.true;
    });

    it('should discard values of other types', () => {
      const setting = new IntSetting(storage, 'discard-values-of-other-types');

      expect(setting.validate(true)).to.be.false;
      expect(setting.validate(false)).to.be.false;
      expect(setting.validate('string')).to.be.false;
      expect(setting.validate(42.4)).to.be.false;
      expect(setting.validate({})).to.be.false;
    });
  });

  describe('parse', () => {
    const setting = new IntSetting(storage, 'parse-integer');

    it('should parse valid string representation of integers', () => {
      expect(setting.parse('42')).not.to.be.null;
      expect(setting.parse('+42')).not.to.be.null;
      expect(setting.parse('-42')).not.to.be.null;
    });

    it('should discard malformed integer strings', () => {
      expect(setting.parse('42.0')).to.be.null;
      expect(setting.parse('-+42')).to.be.null;
    });
  });

  describe('range validation', () => {
    context('empty range', () => {
      it('should discard any value', () => {
        const setting = new IntSetting(storage, 'empty-range', 0, {
          range: {
            lo: 10,
            hi: 5
          }
        });

        expect(setting.validate(0)).to.be.false;
        expect(setting.validate(7)).to.be.false;
        expect(setting.validate(15)).to.be.false;
      });
    });

    context('degenerate range', () => {
      it('should accept only one value', () => {
        const setting = new IntSetting(storage, 'degenerate-range', 0, {
          range: {
            lo: 0,
            hi: 0
          }
        });

        expect(setting.validate(0)).to.be.true;
        expect(setting.validate(42)).to.be.false;
      });
    });

    context('proper closed', () => {
      it('should accept values from the range', () => {
        const setting = new IntSetting(storage, 'proper-closed', 0, {
          range: {
            lo: 5,
            hi: 10
          }
        });

        expect(setting.validate(0)).to.be.false;
        expect(setting.validate(7)).to.be.true;
        expect(setting.validate(5)).to.be.true;
        expect(setting.validate(10)).to.be.true;
        expect(setting.validate(15)).to.be.false;
      });
    });

    context('one-side unbound', () => {
      it('should accept values from the range', () => {
        let setting = new IntSetting(storage, 'one-side-unbound', 0, {
          range: {
            lo: 5
          }
        });

        expect(setting.validate(0)).to.be.false;
        expect(setting.validate(5)).to.be.true;
        expect(setting.validate(100500)).to.be.true;

        setting.range({
          hi: 10
        });

        expect(setting.validate(15)).to.be.false;
        expect(setting.validate(10)).to.be.true;
        expect(setting.validate(-100500)).to.be.true;
      });
    });
  });
});


describe('BoolSetting', () => {
  const storage = new MockStorage();

  it('should have default value false', () => {
    expect(new BoolSetting(storage, 'default').value).to.be.false;
  });

  describe('type validation', () => {
    it('should accept booleans', () => {
      const setting = new BoolSetting(storage, 'accept-integers');

      expect(setting.validate(true)).to.be.true;
      expect(setting.validate(false)).to.be.true;
    });

    it('should discard values of other types', () => {
      const setting = new BoolSetting(storage, 'discard-values-of-other-types');

      expect(setting.validate('string')).to.be.false;
      expect(setting.validate(42.4)).to.be.false;
      expect(setting.validate({})).to.be.false;
    });
  });

  describe('parse', () => {
    const setting = new BoolSetting(storage, 'bool-parse');

    it('should parse booleans', () => {
      expect(setting.parse('true')).to.be.true;
      expect(setting.parse('false')).to.be.false;
    });

    it('should discard other values', () => {
      expect(setting.parse(' true')).to.be.null;
      expect(setting.parse('42')).to.be.null;
      expect(setting.parse('foo')).to.be.null;
    });
  });
});
