'use strict'

/**
 * Class representing a generic setting which can be persisted by {@link Storage}.
 *
 * @access public
 */
export class Setting {
  /**
   * Hook to run after changing the setting.
   * @typedef {function(newVal: *, oldVal: *, setting: Setting)} ChangeSettingHook
   */

  /**
   * Function to validate a new value.
   *
   * The new value considered invalid if the validator returns false or throws, and valid
   * if the validator returns true.
   * @typedef {function(value: *): boolean} SettingValidator
   */

  /**
   * Function to parse a string into a setting's value
   * @typedef {function(input: string): *} SettingParser
   */

  /**
   * Create a new setting.
   *
   * @param {Storage} storage - storage backend.
   * @param {string} key - a unique key which identifies the setting.
   * @param {*} init - initial value of the setting.
   * @param {Object} [options={}] - additional options.
   * @param {string} [options.label] - a string describing the purpose of the setting.
   * @param {string} [options.unit] - a unit of measurement of the setting.
   * @param {boolean} [options.persistent=true] - indicate whether the setting needs to be
   * persisted by the storage.
   */
  constructor(storage, key, init, options={}) {
    this._storage = storage
    this._key = key
    this._init = this._value = init
    this._validators = []
    this._onChangeHooks = []

    this._label = options.label
    this._unit = options.unit
    this._persistent = typeof options.persistent === 'boolean' ? options.persistent : true

    this._storage.registerSetting(this)
  }

  /**
   * A string describing the purpose of the setting.
   *
   * @type {string}
   */
  get label() {
    return this._label || this.key
  }

  /**
   * Initial value of the setting.
   *
   * @type {*}
   */
  get init() {
    return this._init
  }

  /**
   * A unique key which identifies the setting in the backend storage.
   *
   * @type {string}
   */
  get key() {
    return this._key
  }

  /**
   * Indicate whether the setting needs to be persisted by the storage.
   *
   * @type {boolean}
   */
  get persistent() {
    return this._persistent;
  }

  /**
   * An optional unit of measurement of the setting.
   *
   * @type {string}
   */
  get unit() {
    return this._unit || ''
  }

  /**
   * Register a validator function.
   *
   * @param {SettingValidator} validator
   */
  validator(validator) {
    this._validators.push(validator)
  }

  /**
   * Validate a new value.
   *
   * Run all registered validators on the new value.
   * @param {*} value - a new value that needs to be validated
   * @return {boolean} true if the value if valid, false otherwise
   */
  validate(value) {
    try {
      for (let validator of this._validators) {
        if (!validator(value)) {
          return false
        }
      }
    } catch (_e) {
      return false
    }
    return true
  }

  /**
   * The current setting's value.
   */
  get value() {
    return this._value
  }

  /**
   * Set a new setting's value.
   *
   * Do nothing, if the new value is invalid.
   */
  set value(value) {
    if (this.validate(value)) {
      this._value = value
    }
  }

  /**
   * Parse a new value.
   *
   * @return {*} the parsed value or null, if the parsing failed
   */
  parse(value) {
    if (typeof this._settingParser === 'function') {
      return this._settingParser(value)
    }
    return value
  }

  /**
   * Set the setting's parser.
   *
   * @param {SettingParser} parser
   */
  parser(parser) {
    this._settingParser = parser
  }

  /**
   * Load previously stored value from the storage.
   *
   * Do nothing if {@link Setting#persistent} is false.
   */
  load() {
    if (!this.persistent) {
      return
    }

    const value = this._storage.load(this)
    if (value === null) {
      return
    }

    this.value = this.parse(value)
  }

  /**
   * Store the current value to the storage.
   *
   * Do nothing if {@link Setting#persistent} is false.
   */
  save() {
    if (!this.persistent) {
      return
    }

    this._storage.store(this)
  }

  /**
   * Attempt to change the setting's value.
   *
   * Runs all hooks previously registered via {@link Setting#onChange} when
   * the new value is different from the old one.
   *
   * @param {string} value - a new value.
   * @return {boolean} true on success, false otherwise.
   */
  change(value) {
    const parsedValue = this.parse(value)

    if (parsedValue !== null && this.validate(parsedValue)) {
      const oldValue = this.value
      this._value = parsedValue

      if (parsedValue !== oldValue) {
        for (let hook of this._onChangeHooks) {
          hook(parsedValue, oldValue, this)
        }
      }

      return true
    }

    return false
  }

  /**
   * Add hook run after changing the setting's value.
   *
   * @param {ChangeSettingHook} hook
   */
  onChange(hook) {
    this._onChangeHooks.push(hook)
  }

  /**
   * Render the setting into a DOM element.
   *
   * @return {jQuery}
   */
  render() {
    const row = $('<div class="row"></div>')

    $('<label>', {
      'for': this.key,
      text: this.label
    }).appendTo(row)

    const group = $('<div>', {
      'class': 'input-group'
    }).appendTo(row)

    $('<input>', {
      type: 'text',
      id: this.key,
      'class': 'setting',
      name: this.key,
      value: this.value,
      'data-setting-key': this.key
    }).appendTo(group)

    const setting = this

    $('<span>', {
      'class': 'badge',
      'data-unit': this.unit,
      on: {
        click() {
          const badge = $(this)
          const elem = badge.closest('.input-group').find('.setting')

          if (!elem.hasClass('invalid')) {
            return
          }

          elem.val(setting.value)
          elem.removeClass('invalid')
        }
      }
    }).appendTo(group)

    return row
  }

  /**
   * Extract a value from a DOM element.
   *
   * @param {jQuery} elem
   * @return {*}
   */
  getInputValue(elem) {
    return elem.val()
  }

  /**
   * Set an input value from the setting.
   *
   * @param {jQuery} elem
   */
  setInputValue(elem) {
    elem.val(this.value)
  }

  /**
   * Set validation status of the input.
   *
   * @param {jQuery} elem
   * @param {boolean} status
   */
  setValid(elem, status) {
    elem.toggleClass('invalid', !status)
  }
}

/**
 * Class representing an integer setting.
 */
export class IntSetting extends Setting {
  /**
   * Range bound. {@link Range} {@link Range.lo} {@link Range#lo}
   *
   * It's possible to specify different types of ranges.
   * - empty ([]) - both **range.hi** and **range.lo** are specified and **range.lo** > **range.hi**;
   * - degenerate (single value [**range.lo** = **range.hi**]) - both **range.hi** and **range.lo** are specified and
   *     **range.lo** = **range.hi**;
   * - proper closed ([**range.lo**, **range.lo** + 1, ..., **range.hi**]) - **range.lo** < **range.hi**;
   * - left-unbounded ([-**inf**, **range.hi**]) - only **range.hi** is specified;
   * - right-unbounded ([**range.lo**, +**inf**]) - only **range.lo** is specified;
   * - unbounded ([-**inf**, +**inf**]) - either **range** is not specified or
   *    both **range.lo** and **range.hi** are not specified.
   *
   * @typedef {Object} Range
   * @property {number} [lo] - the left bound of the range
   * @property {number} [hi] - the right bound of the range
   */

  /**
   * Create a new integer setting.
   *
   * @override
   * @param {Storage} storage - storage backend.
   * @param {string} key - a unique key which identifies the setting.
   * @param {number} [init=0] - initial value of the setting.
   * @param {Object} [options={}] - additional options.
   * @param {string} [options.label] - a string describing the purpose of the setting.
   * @param {string} [options.unit] - a unit of measurement of the setting.
   * @param {boolean} [options.persistent=true] - indicate whether the setting needs to be persisted
   * @param {Range} [options.range] - an optional range bound
   */
  constructor(storage, key, init, options={}) {
    super(storage, key, init || 0, options)

    const rangeValidator = (value) => {
      if (!(typeof this._range === 'object' && this._range !== null)) {
        return true
      }

      if (typeof this._range.lo === 'number' && typeof this._range.hi !== 'number') {
        return value >= this._range.lo
      }

      if (typeof this._range.hi === 'number' && typeof this._range.lo !== 'number') {
        return value <= this._range.hi
      }

      if (typeof this._range.hi === 'number' && typeof this._range.lo === 'number') {
        return value >= this._range.lo && value <= this._range.hi
      }

      if (typeof this._range.hi !== 'number' && typeof this._range.lo !== 'number') {
        return true
      }

      return false
    }

    this.range(options.range)

    this.validator(Number.isInteger)
    this.validator(rangeValidator)

    this.parser(value => /^[\-\+]?\d+$/.test(value) ? Number(value) : null)
  }

  /**
   * Set a range bound for the setting.
   *
   * @param {Range} range
   */
  range(range) {
    this._range = range
  }
}

/**
 * Class representing an on/off setting.
 */
export class BoolSetting extends Setting {
  /**
   * Create a new boolean setting.
   *
   * @override
   * @param {Storage} storage - storage backend.
   * @param {string} key - a unique key which identifies the setting.
   * @param {boolean} [init=false] - initial value of the setting.
   * @param {Object} [options={}] - additional options.
   * @param {string} [options.label] - a string describing the purpose of the setting.
   * @param {string} [options.unit] - a unit of measurement of the setting.
   * @param {boolean} [options.persistent=true] - indicate whether the setting needs to be persisted
   * by the storage
   */
  constructor(storage, key, init, options={}) {
    super(storage, key, init || false, options)

    this.parser((value) => {
      if (isBoolean(value)) {
        return value
      }

      const values = {
        'true': true,
        'false': false
      }

      let result = values[value]

      if (result === undefined) {
        result = null
      }

      return result;
    })

    this.validator(isBoolean)

    function isBoolean(value) {
      return typeof value === 'boolean'
    }
  }

  /**
   * Render the setting into a DOM element.
   *
   * @override
   * @return {jQuery}
   */
  render() {
    const row = $('<div class="row"></div>')

    $('<label>', {
      'for': this.key,
      text: this.label
    }).appendTo(row)

    const group = $('<div>', {
      'class': 'input-group'
    }).appendTo(row)

    $('<input>', {
      type: 'checkbox',
      id: this.key,
      'class': 'setting',
      name: this.key,
      checked: this.value,
      'data-setting-key': this.key
    }).appendTo(group)

    $('<label>', {
      'class': 'checkbox-overlay',
      'for': this.key
    }).appendTo(group)

    return row
  }

  /**
   * Extract a value from a DOM element.
   *
   * @override
   * @param {jQuery} elem
   * @return {*}
   */
  getInputValue(elem) {
    return elem.is(':checked')
  }

  /**
   * Set an input value from the setting.
   *
   * @override
   * @param {jQuery} elem
   */
  setInputValue(elem) {
    elem.prop('checked', this.value)
  }
}
