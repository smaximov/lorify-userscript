'use strict'

import { Comment, CommentCache } from './comment'
import { LocalStorage } from './storage'
import { IntSetting, BoolSetting } from './setting'
import { FAVICON_NOTIFY, FAVICON_ORIGINAL } from './favicon'

const storage = new LocalStorage()

let autorefreshTickIntervalMillisecs = 1000

let autorefreshEnabled = new BoolSetting(storage, 'autorefreshEnabled', true, {
  label: 'Автоматическое обновление',
  persistent: false
})

const autorefreshIntervalSecs = new IntSetting(storage, 'autorefreshIntervalSecs', 10, {
  label: 'Интервал обновления',
  unit: 'сек',
  range: {
    lo: 5,
    hi: 36000
  }
})

let autorefreshLeftSecs = autorefreshIntervalSecs.value

autorefreshIntervalSecs.onChange((value) => {
  if (autorefreshEnabled.value) {
    autorefreshLeftSecs = value
  }
  updateAutorefreshLabel()
})

autorefreshEnabled.onChange((value) => {
  if (value) {
    autorefreshLeftSecs = autorefreshIntervalSecs.value
  }
  updateAutorefreshLabel()
})

let autorefreshLabel = null
let newCommentsCount = 0
let originalDocumentTitle = ''

const delayBeforePreviewMillisecs = new IntSetting(storage, 'delayBeforePreviewMillisecs', 0, {
  label: 'Задержка перед предпросмотром',
  unit: 'мсек',
  range: {
    lo: 200,
    hi: 4000
  }
})

let delayAfterPreviewMillisecs = 800
let responsesAddingInProcess = false
let commentCache = new CommentCache()
let currentPreview = null
let settingsForm = null

function getCommentInfo(comment) {
  comment = $(comment)
  const replies = comment.find('div.msg-container > div.msg_body > div.reply')
  const commentLinkElement = $(replies.find('a:contains(Ссылка)').first()
  )
  const commentLink = commentLinkElement.attr('href')
  if (typeof commentLink === 'undefined') {
    return null
  }

  const matches = commentLink.match(/.*cid=(\d+).*/)
  if (matches === null) {
    return null
  }
  const commentID = matches[1]

  let referencedCommentID = null
  const a = comment.find('div.title > a').first()
  if (a.length > 0) {
    const referencedCommentLink = a.attr('href')
    if (typeof referencedCommentLink !== 'undefined') {
      const matches = referencedCommentLink.match(/.*cid=(\d+).*/)
      if (matches !== null) {
        referencedCommentID = matches[1]
      }
    }
  }

  const creatorTag = comment.find('a[itemprop=creator]').first()
  const author = creatorTag.length > 0 ? creatorTag.text() : null

  return new Comment(comment, commentID, commentLink, referencedCommentID, author)
}

function addResponsesLinksInternal(comments) {
  for (let comment of comments) {
    commentCache.add(comment)
  }

  for (let commentInfo of comments) {
    if (commentInfo === null) {
      continue
    }

    const cachedCommentInfo = commentCache.find(commentInfo.referencedCommentID)
    if (cachedCommentInfo === null) {
      continue
    }

    if (cachedCommentInfo.element.find(`a.response[href="${commentInfo.link}"]`).length > 0) {
      continue
    }

    const responseTag = $('<a/>', {
      href: commentInfo.link,
      html: commentInfo.author ? commentInfo.author : '>>' + commentInfo.id,
      class: 'response',
      style: 'padding-left: 5px'
    })
    responseTag.click(function(e) {
      const comment = $(`#comment-${commentInfo.id}`).get(0)
      if (comment) {
        e.preventDefault()
        comment.scrollIntoView()
      }
    })

    const replyPanel = cachedCommentInfo.element.find('div.reply > ul').first()
    if (replyPanel.length === 0) {
      continue
    }
    let responseBlock = replyPanel.find('.response-block').first()
    if (responseBlock.length === 0) {
      responseBlock = $('<li/>', {
        html: 'Ответы:',
        class: 'response-block'
      })
      replyPanel.append(' ').append(responseBlock)
    }

    setShowPreviewCallback(responseTag)
    responseBlock.append(responseTag)
  }
}

function getPageComments(page) {
  return $(page)
    .find('#comments article.msg')
    .map((_idx, comment) => getCommentInfo(comment))
}

function addResponsesLinks() {
  if (responsesAddingInProcess) {
    return
  }
  responsesAddingInProcess = true

  // Process current page
  addResponsesLinksInternal(getPageComments(document))

  // Process other pages
  const otherPagesRequests = []
  do {
    const pagesNavBar = $('#comments').find('.nav').first()
    if (pagesNavBar.length === 0) {
      break
    }

    const otherPagesLinksElements = pagesNavBar.find('a.page-number')
    for (let i = 0; i < otherPagesLinksElements.length; ++i) {
      let otherPageLinkElement = $(otherPagesLinksElements[i])

      const otherPageLink = otherPageLinkElement.attr('href')
      if (typeof otherPageLink === 'undefined') {
        continue
      }

      otherPagesRequests.push($.get(otherPageLink))
    }
  } while (false)

  if (otherPagesRequests.length === 0) {
    responsesAddingInProcess = false
  } else {
    const defer = $.when.apply($, otherPagesRequests)
    defer.done(function() {
      $.each(arguments, function(index, responseData) {
        addResponsesLinksInternal(getPageComments(responseData[0]))
      })
    }).always(function() {
      responsesAddingInProcess = false
    })
  }
}

function getOffset(element, offsetType) {
  let offset = 0
  while (element) {
    offset += element[offsetType]
    element = element.offsetParent
  }
  return offset
}

function getScreenWidth() {
  return document.body.clientWidth || document.documentElement.clientWidth
}

function getScreenHeight() {
  return  window.innerHeight || document.documentElement.clientHeight
}

function removeElement(element) {
  if (!element) {
    return
  }

  if (element.parentNode) {
    element.parentNode.removeChild(element)
  }
}

function removePreviews(e) {
  currentPreview = e.relatedTarget
  if (!currentPreview) {
    return
  }

  while (true) {
    if (/^preview/.test(currentPreview.id)) {
      break
    } else {
      currentPreview = currentPreview.parentNode
      if (!currentPreview) {
        break
      }
    }
  }

  setTimeout(function() {
    if (currentPreview === null) {
      $('article.msg[id*="preview-"]').remove()
    } else {
      while (currentPreview.nextSibling) {
        if (!/^preview/.test(currentPreview.nextSibling.id)) {
          break
        }
        removeElement(currentPreview.nextSibling)
      }
    }
  }, delayAfterPreviewMillisecs)
}

function showCommentInternal(commentElement, commentID, e) {
  currentPreview = commentElement.get(0)

  // Avoid duplicated IDs when the original comment was found on the same page
  commentElement.attr('id', `preview-${commentID}`)

  // From makaba
  const hoveredLink = e.target
  const x = getOffset(hoveredLink, 'offsetLeft') + hoveredLink.offsetWidth / 2
  let y = getOffset(hoveredLink, 'offsetTop')
  const screenWidth = getScreenWidth()
  const screenHeight = getScreenHeight()
  if (e.clientY < screenHeight * 0.75) {
    y += hoveredLink.offsetHeight
  }
  commentElement.attr(
    'style',
      'position: absolute;' +
      // There are no limitations for the 'z-index' in the CSS standard,
      // so it depends on the browser. Let's just set it to 300
      'z-index: 300;' +
      'border: 1px solid grey;' +
      (
        x < screenWidth / 2
        ? 'left: ' + x
        : 'right: ' + parseInt(screenWidth - x + 2)
      ) + 'px;' +
      (
        e.clientY < screenHeight * 0.75
        ? 'top: ' + y
        : 'bottom: ' + parseInt(screenHeight - y - 4)
      ) + 'px;'
  )

  // If this comment contains link to another comment,
  // set the 'mouseover' hook to that 'a' tag
  const a = commentElement.find('div.title > a').first()
  if (a.length > 0) {
    setShowPreviewCallback(a)

    let referencedCommentID = -1
    const referencedCommentLink = a.attr('href')
    if (typeof referencedCommentLink !== 'undefined') {
      const matches = referencedCommentLink.match(/.*cid=(\d+).*/)
      if (matches !== null) {
        referencedCommentID = matches[1]
      }
    }
    if (referencedCommentID !== -1) {
      a.click(function(e) {
        const comment = $(`#comment-${referencedCommentID}`).get(0)
        if (comment) {
          e.preventDefault()
          comment.scrollIntoView()
        }
      })
    }
  }

  commentElement.mouseenter(function() {
    if (!currentPreview) {
      currentPreview = this
    }
  })

  commentElement.mouseleave(function(e) {
    removePreviews(e)
  })

  // Note that we append the comment to the '#comments' tag,
  // not the document's body
  // This is because we want to save the background-color and other styles
  // which can be customized by userscripts and themes
  $('#comments').find('article.msg').last().after(commentElement)
}

function showPreview(e) {
  // Extract link to the comment
  const href = $(e.target).attr('href')
  if (typeof href === 'undefined') {
    return
  }

  // Extract comment's ID from the 'href' attribute
  const matches = href.match(/.*cid=(\d+).*/)
  if (matches === null) {
    return
  }
  const commentID = matches[1]

  // Let's reduce an amount of GET requests
  // by searching a cache of comments first
  const comment = commentCache.find(commentID)
  const commentElement = comment ? comment.element : null

  if (commentElement !== null) {
    // Without the 'clone' call we'll just move the original comment
    showCommentInternal(commentElement.clone(true), commentID, e)
    return
  }

  // Get an HTML containing the comment
  $.get(href, function(data) {
    // Search for the comment on the requested page
    const commentElementSelector = 'article.msg[id=comment-' + commentID + ']'
    const commentElement = $(data).find(commentElementSelector).first()
    if (commentElement.length === 0) {
      return
    }

    showCommentInternal(commentElement, commentID, e)
  })
}

function setShowPreviewCallback(elements) {
  elements.hover(function(e) {
    $(this).data('timeout', setTimeout(() => showPreview(e), delayBeforePreviewMillisecs.value))
  }, function(e) {
    clearTimeout($(this).data('timeout'))
    removePreviews(e)
  })
}

function changingSettings() {
  return settingsForm !== null && settingsForm.is(':visible')
}

function updateAutorefreshLabel() {
  let newLabel = autorefreshEnabled.value
        ? `Автообновление через ${autorefreshLeftSecs} с...`
        : 'Автообновление'

  if (changingSettings()) {
    newLabel += ' (пауза)'
  }

  autorefreshLabel.find('span').text(newLabel)
}

function autorefreshTick() {
  if (!autorefreshEnabled.value || changingSettings()) {
    updateAutorefreshLabel()
    setTimeout(autorefreshTick, autorefreshTickIntervalMillisecs)
    return
  }

  autorefreshLeftSecs -= 1
  if (autorefreshLeftSecs !== 0) {
    updateAutorefreshLabel()
    setTimeout(autorefreshTick, autorefreshTickIntervalMillisecs)
    return
  }

  autorefreshLabel.find('span').text('Обновление...')

  const comments = $('#comments')

  $.get(document.location.href, function(data) {
    const messages = $(data).find('article.msg')
    const notifications_count_new = $(data).find('#main_events_count')
    const notifications_count_old = $("#main_events_count")

    if (notifications_count_new.text() !== notifications_count_old.text()) {
      notifications_count_old.text(notifications_count_new.text())
    }

    for (let i = 0; i < messages.length; ++i) {
      const message = $(messages[i])
      const messageID = message.attr('id')
      const isNewMessage = $('#' + messageID).length === 0
      if (!isNewMessage) {
        continue
      }

      ++newCommentsCount

      const a = message.find('div.title > a').first()
      if (a.length > 0) {
        setShowPreviewCallback(a)
        let referencedCommentID = -1
        const referencedCommentLink = a.attr('href')
        if (typeof referencedCommentLink !== 'undefined') {
          const matches = referencedCommentLink.match(/.*cid=(\d+).*/)
          if (matches !== null) {
            referencedCommentID = matches[1]
          }
        }
        if (referencedCommentID !== -1) {
          a.click(function(e) {
            const comment = $('#comment-' + referencedCommentID).get(0)
            if (comment) {
              e.preventDefault()
              comment.scrollIntoView()
            }
          })
        }
      }

      comments.append(message)
    }

    // Remove all nav. bars related to the pagination
    // (there are also nav. bars related to threads, but let's just skip them)
    let pagesBefore = 0
    const currentNavBars = $(document.body).find('.nav')
    for (let i = 0; i < currentNavBars.length; ++i) {
      const currentNavBar = $(currentNavBars[i])
      const pagesElementsCount = currentNavBar.find('.page-number').length
      const isPagesNavBar = pagesElementsCount !== 0
      if (isPagesNavBar) {
        pagesBefore = pagesElementsCount
        currentNavBar.remove()
      }
    }

    let pagesAfter = 0
    const newPagesNavBar = $(data).find('#comments').find('.nav').has('.page-number').first()
    if (newPagesNavBar.length === 0) {
      comments.append(autorefreshLabel)
      comments.append(settingsForm)
    } else {
      pagesAfter = newPagesNavBar.find('.page-number').length
      comments.find('.msg').first().before(newPagesNavBar.clone())
      comments.append(autorefreshLabel)
      comments.append(settingsForm)
      comments.append(newPagesNavBar.clone())
    }

    if (pagesAfter > pagesBefore || newCommentsCount !== 0) {
      faviconNotify(true)
    }

    if (pagesAfter > pagesBefore) {
      document.title = `(!) ${originalDocumentTitle}`
    } else if (newCommentsCount !== 0) {
      document.title = `(${newCommentsCount}) ${originalDocumentTitle}`
    }

    addResponsesLinks()
  }).always(function() {
    autorefreshLeftSecs = autorefreshIntervalSecs.value
    updateAutorefreshLabel()

    setTimeout(autorefreshTick, autorefreshTickIntervalMillisecs)
  })
}

function setAutorefresher() {
  originalDocumentTitle = document.title

  autorefreshLabel = $('<div/>', {
    html: '<span></span>',
    style: 'margin: 1em auto;'
  })
  $('<button>', {
    type: 'button',
    class: 'btn btn-default btn-small',
    text: 'Настройки',
    style: 'float: right;',
    on: {
      click() {
        settingsForm.slideToggle()
      }
    }
  }).appendTo(autorefreshLabel)

  updateAutorefreshLabel()
  $('#comments').append(autorefreshLabel)

  $(window).scroll(function() {
    if (newCommentsCount !== 0) {
      faviconNotify(false)
      document.title = originalDocumentTitle
      newCommentsCount = 0
    }
  })

  // setInterval calls may overlap so let's call setTimeot each time manually
  setTimeout(autorefreshTick, autorefreshTickIntervalMillisecs)
}

function removePreviewsOnClick() {
  $(window).click(function() {
    const previewSelector = 'article.msg[id*="preview-"]'
    if (currentPreview === null) {
      $(previewSelector).remove()
    }
  })
}

const archivedTopicString = 'Вы не можете добавлять комментарии в эту тему. Тема перемещена в архив.'
const deletedTopicString = 'Вы не можете добавлять комментарии в эту тему. Тема удалена.'

function disableAutorefreshForClosedTopics() {
  const infoblock = $('.infoblock').text()

  if (infoblock.indexOf(archivedTopicString) !== -1 ||
      infoblock.indexOf(deletedTopicString) !== -1) {
    autorefreshEnabled.value = false
  }
}

const settingsFormStyle = `
#settings {
  padding: 0.5em;
  display: flex;
  flex-direction: column;
  justify-content: around;
}

#settings .row {
  margin: 0.5em;
}

#settings .row > label {
  float: left;
  text-align: right;
  width: 50%;
}

#settings input[type="text"] {
  border: none;
  padding: 5px;
  padding-right: 3em;
  margin: auto 1em;
  background-color: transparent;
  color: inherit;
  border-bottom: 1px solid #707070;
  transition: border 0.4s ease-in-out;
}

#settings input[type="checkbox"] {
  margin: auto 1em;
}

#settings input[type="text"]:hover {
  border-color: #b0b0b0;
}

#settings input[type="text"]:focus {
  outline: none;
  border-color: #729fcf;
}

#settings .input-group {
  float: left;
  position: relative;
}

#settings .input-group .badge::after {
  position: absolute;
  content: attr(data-unit);
  opacity: 0.6;
  right: calc(1em + 5px);
  top: 2.5px;
  transition: opacity 0.4s ease-in-out, color 0.4s ease-in-out;
}

#settings .input-group:hover .badge::after {
  opacity: 1.0;
}

#settings .input-group input[type="text"]:focus + .badge::after {
  opacity: 1.0;
}

#settings .input-group input[type="text"].invalid + .badge::after {
  opacity: 1.0;
  content: '\\27f2';
  color: #729fcf;
}

#settings .input-group input[type="text"].invalid + .badge:hover::after {
  cursor: pointer;
}

#settings input[type="checkbox"] {
  visibility: hidden;
}

#settings .checkbox-overlay {
  position: absolute;
  width: 18px;
  height: 18px;
  cursor: pointer;
  top: 0;
  left: 0;
  margin: auto 1em;
  margin-top: 4px;
  background: transparent;
  border: 1px solid #707070;

  transition: border 0.4s ease-in-out;
}

#settings .checkbox-overlay::after {
  opacity: 0.1;
  content: '\\2713';
  position: relative;
  bottom: 3px;
  left: 1.5px;
  color: inherit;
  font-size: 110%;
  background: transparent;

  transition: opacity 0.2s;
}

#settings .checkbox-overlay:hover::after {
  opacity: 0.5;
}

#settings input[type="checkbox"]:checked + .checkbox-overlay::after {
  opacity: 1.0;
}

#settings .checkbox-overlay:hover {
  border-color: #b0b0b0;
}

#settings button {
  margin: auto 1em;
}

#settings input[type="text"].invalid,
#settings input[type="checkbox"].invalid + .checkbox-overlay {
  border-color: red;
}
`

function setupSettingsForm() {
  settingsForm = $('<form>', {
    id: 'settings',
    style: 'display: none;',
    on: {
      submit(ev) {
        ev.preventDefault()

        let failed = false

        settingsForm.find('.setting').each((_, elem) => {
          elem = $(elem)
          const key = elem.data('setting-key')
          const setting = storage.get(key)
          const value = setting.getInputValue(elem)
          const success = setting.change(value)
          setting.setValid(elem, success)

          failed = failed || !success
        })

        storage.saveSettings()

        if (!failed) {
          const button = settingsForm.find('button')
          button.text('Готово!')

          setTimeout(() => {
            settingsForm.slideUp(() => {
              button.text('Сохранить')
            })
          }, 500)
        }
      }
    }
  })

  for (let setting of storage) {
    settingsForm.append(setting.render())
  }

  const row = $('<div class="row"></div>')
  $('<label>').appendTo(row)
  $('<button>', {
    'class': 'btn btn-default',
    type: 'submit',
    text: 'Сохранить'
  }).appendTo(row)

  settingsForm.append(row)

  $('<style>', {
    html: settingsFormStyle
  }).appendTo(settingsForm)

  $('#comments').append(settingsForm)
}

function loadSettings() {
  storage.loadSettings()

  autorefreshLeftSecs = autorefreshIntervalSecs.value
}

let favicon = null

function getFavicon() {
  if (favicon === null) {
    favicon = $('head > link[rel~="icon"]')
  }
  return favicon
}

function faviconNotify(flag) {
  getFavicon().attr('href', flag ? FAVICON_NOTIFY : FAVICON_ORIGINAL)
}

function onDOMReady() {
  loadSettings()
  removePreviewsOnClick()
  setShowPreviewCallback($('div.title > a'))
  disableAutorefreshForClosedTopics()
  setAutorefresher()
  setupSettingsForm()
  addResponsesLinks()
}

$(onDOMReady)
