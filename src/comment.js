'use strict'

/**
 * @external {jQuery} https://api.jquery.com/Types/#jQuery
 */

/**
 * Class representing a thread comment.
 */
export class Comment {
  /**
   * Create a new {@link Comment}.
   *
   * @param {jQuery} element - jQuery wrapper around the comment's DOM element
   * @param {string} id - comment's ID
   * @param {string} link - comment's URL
   * @param {?string} referencedCommentID - ID of the comment's parent, if any
   * @param {?string} author - author of the comment, if any
   */
  constructor(element, id, link, referencedCommentID, author) {
    /**
     * jQuery wrapper around the comment's DOM element.
     * @type {jQuery}
     */
    this.element = element

    /**
     * Comment's ID.
     * @type {string}
     */
    this.id = id

    /**
     * Comment's URL.
     * @type {string}
     */
    this.link = link

    /**
     * ID of the comment's parent, if any.
     * @type {?string}
     */
    this.referencedCommentID = referencedCommentID


    /**
     * Author of the comment, if any.
     * @type {?string}
     */
    this.author = author
  }
}

/**
 * A cache which stores comments by {@link Comment#id}
 */
export class CommentCache {
  /**
   * Create a new empty cache.
   */
  constructor() {
    this._cache = {}
  }

  /**
   * Add a comment to the cache.
   *
   * @param {Comment} comment
   */
  add(comment) {
    this._cache[comment.id] = comment
  }

  /**
   * Find a comment by ID
   *
   * @param {string} id
   */
  find(id) {
    return this._cache[id] || null
  }
}
