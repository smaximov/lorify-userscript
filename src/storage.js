'use strict'

/**
 * An abstract storage class.
 *
 * It keeps track of all registered settings by {@link Setting#key}.
 */
export class Storage {
  /**
   * Create a new storage.
   *
   * This method MUST be called in every subclass that overrides the default constructor.
   *
   * @example
   *
   * class MyStorage extends Storage {
   *   // Custom constructor:
   *   constructor() {
   *     super() // Call the Storage's constructor
   *   }
   * }
   */
  constructor() {
    if (new.target === Storage) {
      throw new TypeError("Cannot directly create an instance of Storage.")
    }

    this._settings = {}
  }

  /**
   * Add a new {@link Setting} to the storage's settings registry.
   *
   * @param {Setting} setting
   */
  registerSetting(setting) {
    this._settings[setting.key] = setting
  }

  /**
   * Load all registered settings from the storage.
   */
  loadSettings() {
    for (let setting of this) {
      setting.load()
    }
  }

  /**
   * Save all registered settings to the storage.
   */
  saveSettings() {
    for (let setting of this) {
      setting.save()
    }
  }

  /**
   * Get a {@link Setting} by key.
   *
   * @param {string} key
   * @return {Setting}
   */
  get(key) {
    return this._settings[key]
  }

  /**
   * Return a generator to iterate over all registered settings.
   *
   * @example
   * for (let setting of storage) {
   *   ...
   * }
   */
  *[Symbol.iterator]() {
    for (let key in this._settings) {
      yield this._settings[key]
    }
  }

  /**
   * Store the setting in the storage.
   *
   * @abstract
   * @param {Setting} setting
   */
  store(setting) {
    setting = setting           // Ignore eslint's complains about unused variable
    throw new Error('unimplemented')
  }

  /**
   * Return the value of previously stored setting.
   *
   * @abstract
   * @param {Setting} setting
   * @return {*} previously stored value or null, if none
   */
  load(setting) {
    setting = setting           // Ignore eslint's complains about unused variable
    throw new Error('unimplemented')
  }
}

/**
 * Persist settings via browser's Local Storage.
 */
export class LocalStorage extends Storage {
  /**
   * Indicate whether the browser supports local storage.
   * @see https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API/Using_the_Web_Storage_API#Testing_for_support_vs_availability
   */
  static get available() {
   try {
     const storage = window.localStorage
     const x = '__storage_test__'

     storage.setItem(x, x)
     storage.removeItem(x)

     return true
   } catch(e) {
     return false
   }
  }

  /**
   * Store the setting in Local Storage.
   *
   * @override
   * @param {Setting} setting
   */
  store(setting) {
    if (LocalStorage.available) {
      localStorage.setItem(setting.key, setting.value)
    }
  }

  /**
   * Return the value of previously stored setting.
   *
   * @override
   * @param {Setting} setting
   * @return {*} previously stored value or null, if none
   */
  load(setting) {
    if (LocalStorage.available) {
      return localStorage.getItem(setting.key)
    }

    return null
  }
}
