// ==UserScript==
// @name        lorify
// @namespace   https://bitbucket.org/b0r3d0m/lorify-userscript
// @description lorify provides you an autorefresh for threads and an easy way to view referenced comments on linux.org.ru's forums
// @include     /^https?:\/\/www.linux.org.ru\/forum\/.*$/
// @include     /^https?:\/\/www.linux.org.ru\/gallery\/.*$/
// @include     /^https?:\/\/www.linux.org.ru\/news\/.*$/
// @version     1.11.0
// @grant       none
// @require     http://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js
// @icon        http://icons.iconarchive.com/icons/icons8/christmas-flat-color/32/penguin-icon.png
// @updateURL   https://bitbucket.org/smaximov/lorify-userscript/raw/master/lorify.meta.js
// @downloadURL https://bitbucket.org/smaximov/lorify-userscript/raw/master/lorify.user.js
// ==/UserScript==

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _getIterator2 = __webpack_require__(1);

	var _getIterator3 = _interopRequireDefault(_getIterator2);

	var _comment = __webpack_require__(39);

	var _storage = __webpack_require__(44);

	var _setting = __webpack_require__(90);

	var _favicon = __webpack_require__(95);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var storage = new _storage.LocalStorage();

	var autorefreshTickIntervalMillisecs = 1000;

	var autorefreshEnabled = new _setting.BoolSetting(storage, 'autorefreshEnabled', true, {
	  label: 'Автоматическое обновление',
	  persistent: false
	});

	var autorefreshIntervalSecs = new _setting.IntSetting(storage, 'autorefreshIntervalSecs', 10, {
	  label: 'Интервал обновления',
	  unit: 'сек',
	  range: {
	    lo: 5,
	    hi: 36000
	  }
	});

	var autorefreshLeftSecs = autorefreshIntervalSecs.value;

	autorefreshIntervalSecs.onChange(function (value) {
	  if (autorefreshEnabled.value) {
	    autorefreshLeftSecs = value;
	  }
	  updateAutorefreshLabel();
	});

	autorefreshEnabled.onChange(function (value) {
	  if (value) {
	    autorefreshLeftSecs = autorefreshIntervalSecs.value;
	  }
	  updateAutorefreshLabel();
	});

	var autorefreshLabel = null;
	var newCommentsCount = 0;
	var originalDocumentTitle = '';

	var delayBeforePreviewMillisecs = new _setting.IntSetting(storage, 'delayBeforePreviewMillisecs', 0, {
	  label: 'Задержка перед предпросмотром',
	  unit: 'мсек',
	  range: {
	    lo: 200,
	    hi: 4000
	  }
	});

	var delayAfterPreviewMillisecs = 800;
	var responsesAddingInProcess = false;
	var commentCache = new _comment.CommentCache();
	var currentPreview = null;
	var settingsForm = null;

	function getCommentInfo(comment) {
	  comment = $(comment);
	  var replies = comment.find('div.msg-container > div.msg_body > div.reply');
	  var commentLinkElement = $(replies.find('a:contains(Ссылка)').first());
	  var commentLink = commentLinkElement.attr('href');
	  if (typeof commentLink === 'undefined') {
	    return null;
	  }

	  var matches = commentLink.match(/.*cid=(\d+).*/);
	  if (matches === null) {
	    return null;
	  }
	  var commentID = matches[1];

	  var referencedCommentID = null;
	  var a = comment.find('div.title > a').first();
	  if (a.length > 0) {
	    var referencedCommentLink = a.attr('href');
	    if (typeof referencedCommentLink !== 'undefined') {
	      var _matches = referencedCommentLink.match(/.*cid=(\d+).*/);
	      if (_matches !== null) {
	        referencedCommentID = _matches[1];
	      }
	    }
	  }

	  var creatorTag = comment.find('a[itemprop=creator]').first();
	  var author = creatorTag.length > 0 ? creatorTag.text() : null;

	  return new _comment.Comment(comment, commentID, commentLink, referencedCommentID, author);
	}

	function addResponsesLinksInternal(comments) {
	  var _iteratorNormalCompletion = true;
	  var _didIteratorError = false;
	  var _iteratorError = undefined;

	  try {
	    for (var _iterator = (0, _getIterator3.default)(comments), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	      var comment = _step.value;

	      commentCache.add(comment);
	    }
	  } catch (err) {
	    _didIteratorError = true;
	    _iteratorError = err;
	  } finally {
	    try {
	      if (!_iteratorNormalCompletion && _iterator.return) {
	        _iterator.return();
	      }
	    } finally {
	      if (_didIteratorError) {
	        throw _iteratorError;
	      }
	    }
	  }

	  var _iteratorNormalCompletion2 = true;
	  var _didIteratorError2 = false;
	  var _iteratorError2 = undefined;

	  try {
	    var _loop = function _loop() {
	      var commentInfo = _step2.value;

	      if (commentInfo === null) {
	        return 'continue';
	      }

	      var cachedCommentInfo = commentCache.find(commentInfo.referencedCommentID);
	      if (cachedCommentInfo === null) {
	        return 'continue';
	      }

	      if (cachedCommentInfo.element.find('a.response[href="' + commentInfo.link + '"]').length > 0) {
	        return 'continue';
	      }

	      var responseTag = $('<a/>', {
	        href: commentInfo.link,
	        html: commentInfo.author ? commentInfo.author : '>>' + commentInfo.id,
	        class: 'response',
	        style: 'padding-left: 5px'
	      });
	      responseTag.click(function (e) {
	        var comment = $('#comment-' + commentInfo.id).get(0);
	        if (comment) {
	          e.preventDefault();
	          comment.scrollIntoView();
	        }
	      });

	      var replyPanel = cachedCommentInfo.element.find('div.reply > ul').first();
	      if (replyPanel.length === 0) {
	        return 'continue';
	      }
	      var responseBlock = replyPanel.find('.response-block').first();
	      if (responseBlock.length === 0) {
	        responseBlock = $('<li/>', {
	          html: 'Ответы:',
	          class: 'response-block'
	        });
	        replyPanel.append(' ').append(responseBlock);
	      }

	      setShowPreviewCallback(responseTag);
	      responseBlock.append(responseTag);
	    };

	    for (var _iterator2 = (0, _getIterator3.default)(comments), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
	      var _ret = _loop();

	      if (_ret === 'continue') continue;
	    }
	  } catch (err) {
	    _didIteratorError2 = true;
	    _iteratorError2 = err;
	  } finally {
	    try {
	      if (!_iteratorNormalCompletion2 && _iterator2.return) {
	        _iterator2.return();
	      }
	    } finally {
	      if (_didIteratorError2) {
	        throw _iteratorError2;
	      }
	    }
	  }
	}

	function getPageComments(page) {
	  return $(page).find('#comments article.msg').map(function (_idx, comment) {
	    return getCommentInfo(comment);
	  });
	}

	function addResponsesLinks() {
	  if (responsesAddingInProcess) {
	    return;
	  }
	  responsesAddingInProcess = true;

	  // Process current page
	  addResponsesLinksInternal(getPageComments(document));

	  // Process other pages
	  var otherPagesRequests = [];
	  do {
	    var pagesNavBar = $('#comments').find('.nav').first();
	    if (pagesNavBar.length === 0) {
	      break;
	    }

	    var otherPagesLinksElements = pagesNavBar.find('a.page-number');
	    for (var i = 0; i < otherPagesLinksElements.length; ++i) {
	      var otherPageLinkElement = $(otherPagesLinksElements[i]);

	      var otherPageLink = otherPageLinkElement.attr('href');
	      if (typeof otherPageLink === 'undefined') {
	        continue;
	      }

	      otherPagesRequests.push($.get(otherPageLink));
	    }
	  } while (false);

	  if (otherPagesRequests.length === 0) {
	    responsesAddingInProcess = false;
	  } else {
	    var defer = $.when.apply($, otherPagesRequests);
	    defer.done(function () {
	      $.each(arguments, function (index, responseData) {
	        addResponsesLinksInternal(getPageComments(responseData[0]));
	      });
	    }).always(function () {
	      responsesAddingInProcess = false;
	    });
	  }
	}

	function getOffset(element, offsetType) {
	  var offset = 0;
	  while (element) {
	    offset += element[offsetType];
	    element = element.offsetParent;
	  }
	  return offset;
	}

	function getScreenWidth() {
	  return document.body.clientWidth || document.documentElement.clientWidth;
	}

	function getScreenHeight() {
	  return window.innerHeight || document.documentElement.clientHeight;
	}

	function removeElement(element) {
	  if (!element) {
	    return;
	  }

	  if (element.parentNode) {
	    element.parentNode.removeChild(element);
	  }
	}

	function removePreviews(e) {
	  currentPreview = e.relatedTarget;
	  if (!currentPreview) {
	    return;
	  }

	  while (true) {
	    if (/^preview/.test(currentPreview.id)) {
	      break;
	    } else {
	      currentPreview = currentPreview.parentNode;
	      if (!currentPreview) {
	        break;
	      }
	    }
	  }

	  setTimeout(function () {
	    if (currentPreview === null) {
	      $('article.msg[id*="preview-"]').remove();
	    } else {
	      while (currentPreview.nextSibling) {
	        if (!/^preview/.test(currentPreview.nextSibling.id)) {
	          break;
	        }
	        removeElement(currentPreview.nextSibling);
	      }
	    }
	  }, delayAfterPreviewMillisecs);
	}

	function showCommentInternal(commentElement, commentID, e) {
	  currentPreview = commentElement.get(0);

	  // Avoid duplicated IDs when the original comment was found on the same page
	  commentElement.attr('id', 'preview-' + commentID);

	  // From makaba
	  var hoveredLink = e.target;
	  var x = getOffset(hoveredLink, 'offsetLeft') + hoveredLink.offsetWidth / 2;
	  var y = getOffset(hoveredLink, 'offsetTop');
	  var screenWidth = getScreenWidth();
	  var screenHeight = getScreenHeight();
	  if (e.clientY < screenHeight * 0.75) {
	    y += hoveredLink.offsetHeight;
	  }
	  commentElement.attr('style', 'position: absolute;' +
	  // There are no limitations for the 'z-index' in the CSS standard,
	  // so it depends on the browser. Let's just set it to 300
	  'z-index: 300;' + 'border: 1px solid grey;' + (x < screenWidth / 2 ? 'left: ' + x : 'right: ' + parseInt(screenWidth - x + 2)) + 'px;' + (e.clientY < screenHeight * 0.75 ? 'top: ' + y : 'bottom: ' + parseInt(screenHeight - y - 4)) + 'px;');

	  // If this comment contains link to another comment,
	  // set the 'mouseover' hook to that 'a' tag
	  var a = commentElement.find('div.title > a').first();
	  if (a.length > 0) {
	    (function () {
	      setShowPreviewCallback(a);

	      var referencedCommentID = -1;
	      var referencedCommentLink = a.attr('href');
	      if (typeof referencedCommentLink !== 'undefined') {
	        var matches = referencedCommentLink.match(/.*cid=(\d+).*/);
	        if (matches !== null) {
	          referencedCommentID = matches[1];
	        }
	      }
	      if (referencedCommentID !== -1) {
	        a.click(function (e) {
	          var comment = $('#comment-' + referencedCommentID).get(0);
	          if (comment) {
	            e.preventDefault();
	            comment.scrollIntoView();
	          }
	        });
	      }
	    })();
	  }

	  commentElement.mouseenter(function () {
	    if (!currentPreview) {
	      currentPreview = this;
	    }
	  });

	  commentElement.mouseleave(function (e) {
	    removePreviews(e);
	  });

	  // Note that we append the comment to the '#comments' tag,
	  // not the document's body
	  // This is because we want to save the background-color and other styles
	  // which can be customized by userscripts and themes
	  $('#comments').find('article.msg').last().after(commentElement);
	}

	function showPreview(e) {
	  // Extract link to the comment
	  var href = $(e.target).attr('href');
	  if (typeof href === 'undefined') {
	    return;
	  }

	  // Extract comment's ID from the 'href' attribute
	  var matches = href.match(/.*cid=(\d+).*/);
	  if (matches === null) {
	    return;
	  }
	  var commentID = matches[1];

	  // Let's reduce an amount of GET requests
	  // by searching a cache of comments first
	  var comment = commentCache.find(commentID);
	  var commentElement = comment ? comment.element : null;

	  if (commentElement !== null) {
	    // Without the 'clone' call we'll just move the original comment
	    showCommentInternal(commentElement.clone(true), commentID, e);
	    return;
	  }

	  // Get an HTML containing the comment
	  $.get(href, function (data) {
	    // Search for the comment on the requested page
	    var commentElementSelector = 'article.msg[id=comment-' + commentID + ']';
	    var commentElement = $(data).find(commentElementSelector).first();
	    if (commentElement.length === 0) {
	      return;
	    }

	    showCommentInternal(commentElement, commentID, e);
	  });
	}

	function setShowPreviewCallback(elements) {
	  elements.hover(function (e) {
	    $(this).data('timeout', setTimeout(function () {
	      return showPreview(e);
	    }, delayBeforePreviewMillisecs.value));
	  }, function (e) {
	    clearTimeout($(this).data('timeout'));
	    removePreviews(e);
	  });
	}

	function changingSettings() {
	  return settingsForm !== null && settingsForm.is(':visible');
	}

	function updateAutorefreshLabel() {
	  var newLabel = autorefreshEnabled.value ? 'Автообновление через ' + autorefreshLeftSecs + ' с...' : 'Автообновление';

	  if (changingSettings()) {
	    newLabel += ' (пауза)';
	  }

	  autorefreshLabel.find('span').text(newLabel);
	}

	function autorefreshTick() {
	  if (!autorefreshEnabled.value || changingSettings()) {
	    updateAutorefreshLabel();
	    setTimeout(autorefreshTick, autorefreshTickIntervalMillisecs);
	    return;
	  }

	  autorefreshLeftSecs -= 1;
	  if (autorefreshLeftSecs !== 0) {
	    updateAutorefreshLabel();
	    setTimeout(autorefreshTick, autorefreshTickIntervalMillisecs);
	    return;
	  }

	  autorefreshLabel.find('span').text('Обновление...');

	  var comments = $('#comments');

	  $.get(document.location.href, function (data) {
	    var messages = $(data).find('article.msg');
	    var notifications_count_new = $(data).find('#main_events_count');
	    var notifications_count_old = $("#main_events_count");

	    if (notifications_count_new.text() !== notifications_count_old.text()) {
	      notifications_count_old.text(notifications_count_new.text());
	    }

	    for (var i = 0; i < messages.length; ++i) {
	      var message = $(messages[i]);
	      var messageID = message.attr('id');
	      var isNewMessage = $('#' + messageID).length === 0;
	      if (!isNewMessage) {
	        continue;
	      }

	      ++newCommentsCount;

	      var a = message.find('div.title > a').first();
	      if (a.length > 0) {
	        (function () {
	          setShowPreviewCallback(a);
	          var referencedCommentID = -1;
	          var referencedCommentLink = a.attr('href');
	          if (typeof referencedCommentLink !== 'undefined') {
	            var matches = referencedCommentLink.match(/.*cid=(\d+).*/);
	            if (matches !== null) {
	              referencedCommentID = matches[1];
	            }
	          }
	          if (referencedCommentID !== -1) {
	            a.click(function (e) {
	              var comment = $('#comment-' + referencedCommentID).get(0);
	              if (comment) {
	                e.preventDefault();
	                comment.scrollIntoView();
	              }
	            });
	          }
	        })();
	      }

	      comments.append(message);
	    }

	    // Remove all nav. bars related to the pagination
	    // (there are also nav. bars related to threads, but let's just skip them)
	    var pagesBefore = 0;
	    var currentNavBars = $(document.body).find('.nav');
	    for (var i = 0; i < currentNavBars.length; ++i) {
	      var currentNavBar = $(currentNavBars[i]);
	      var pagesElementsCount = currentNavBar.find('.page-number').length;
	      var isPagesNavBar = pagesElementsCount !== 0;
	      if (isPagesNavBar) {
	        pagesBefore = pagesElementsCount;
	        currentNavBar.remove();
	      }
	    }

	    var pagesAfter = 0;
	    var newPagesNavBar = $(data).find('#comments').find('.nav').has('.page-number').first();
	    if (newPagesNavBar.length === 0) {
	      comments.append(autorefreshLabel);
	      comments.append(settingsForm);
	    } else {
	      pagesAfter = newPagesNavBar.find('.page-number').length;
	      comments.find('.msg').first().before(newPagesNavBar.clone());
	      comments.append(autorefreshLabel);
	      comments.append(settingsForm);
	      comments.append(newPagesNavBar.clone());
	    }

	    if (pagesAfter > pagesBefore || newCommentsCount !== 0) {
	      faviconNotify(true);
	    }

	    if (pagesAfter > pagesBefore) {
	      document.title = '(!) ' + originalDocumentTitle;
	    } else if (newCommentsCount !== 0) {
	      document.title = '(' + newCommentsCount + ') ' + originalDocumentTitle;
	    }

	    addResponsesLinks();
	  }).always(function () {
	    autorefreshLeftSecs = autorefreshIntervalSecs.value;
	    updateAutorefreshLabel();

	    setTimeout(autorefreshTick, autorefreshTickIntervalMillisecs);
	  });
	}

	function setAutorefresher() {
	  originalDocumentTitle = document.title;

	  autorefreshLabel = $('<div/>', {
	    html: '<span></span>',
	    style: 'margin: 1em auto;'
	  });
	  $('<button>', {
	    type: 'button',
	    class: 'btn btn-default btn-small',
	    text: 'Настройки',
	    style: 'float: right;',
	    on: {
	      click: function click() {
	        settingsForm.slideToggle();
	      }
	    }
	  }).appendTo(autorefreshLabel);

	  updateAutorefreshLabel();
	  $('#comments').append(autorefreshLabel);

	  $(window).scroll(function () {
	    if (newCommentsCount !== 0) {
	      faviconNotify(false);
	      document.title = originalDocumentTitle;
	      newCommentsCount = 0;
	    }
	  });

	  // setInterval calls may overlap so let's call setTimeot each time manually
	  setTimeout(autorefreshTick, autorefreshTickIntervalMillisecs);
	}

	function removePreviewsOnClick() {
	  $(window).click(function () {
	    var previewSelector = 'article.msg[id*="preview-"]';
	    if (currentPreview === null) {
	      $(previewSelector).remove();
	    }
	  });
	}

	var archivedTopicString = 'Вы не можете добавлять комментарии в эту тему. Тема перемещена в архив.';
	var deletedTopicString = 'Вы не можете добавлять комментарии в эту тему. Тема удалена.';

	function disableAutorefreshForClosedTopics() {
	  var infoblock = $('.infoblock').text();

	  if (infoblock.indexOf(archivedTopicString) !== -1 || infoblock.indexOf(deletedTopicString) !== -1) {
	    autorefreshEnabled.value = false;
	  }
	}

	var settingsFormStyle = '\n#settings {\n  padding: 0.5em;\n  display: flex;\n  flex-direction: column;\n  justify-content: around;\n}\n\n#settings .row {\n  margin: 0.5em;\n}\n\n#settings .row > label {\n  float: left;\n  text-align: right;\n  width: 50%;\n}\n\n#settings input[type="text"] {\n  border: none;\n  padding: 5px;\n  padding-right: 3em;\n  margin: auto 1em;\n  background-color: transparent;\n  color: inherit;\n  border-bottom: 1px solid #707070;\n  transition: border 0.4s ease-in-out;\n}\n\n#settings input[type="checkbox"] {\n  margin: auto 1em;\n}\n\n#settings input[type="text"]:hover {\n  border-color: #b0b0b0;\n}\n\n#settings input[type="text"]:focus {\n  outline: none;\n  border-color: #729fcf;\n}\n\n#settings .input-group {\n  float: left;\n  position: relative;\n}\n\n#settings .input-group .badge::after {\n  position: absolute;\n  content: attr(data-unit);\n  opacity: 0.6;\n  right: calc(1em + 5px);\n  top: 2.5px;\n  transition: opacity 0.4s ease-in-out, color 0.4s ease-in-out;\n}\n\n#settings .input-group:hover .badge::after {\n  opacity: 1.0;\n}\n\n#settings .input-group input[type="text"]:focus + .badge::after {\n  opacity: 1.0;\n}\n\n#settings .input-group input[type="text"].invalid + .badge::after {\n  opacity: 1.0;\n  content: \'\\27f2\';\n  color: #729fcf;\n}\n\n#settings .input-group input[type="text"].invalid + .badge:hover::after {\n  cursor: pointer;\n}\n\n#settings input[type="checkbox"] {\n  visibility: hidden;\n}\n\n#settings .checkbox-overlay {\n  position: absolute;\n  width: 18px;\n  height: 18px;\n  cursor: pointer;\n  top: 0;\n  left: 0;\n  margin: auto 1em;\n  margin-top: 4px;\n  background: transparent;\n  border: 1px solid #707070;\n\n  transition: border 0.4s ease-in-out;\n}\n\n#settings .checkbox-overlay::after {\n  opacity: 0.1;\n  content: \'\\2713\';\n  position: relative;\n  bottom: 3px;\n  left: 1.5px;\n  color: inherit;\n  font-size: 110%;\n  background: transparent;\n\n  transition: opacity 0.2s;\n}\n\n#settings .checkbox-overlay:hover::after {\n  opacity: 0.5;\n}\n\n#settings input[type="checkbox"]:checked + .checkbox-overlay::after {\n  opacity: 1.0;\n}\n\n#settings .checkbox-overlay:hover {\n  border-color: #b0b0b0;\n}\n\n#settings button {\n  margin: auto 1em;\n}\n\n#settings input[type="text"].invalid,\n#settings input[type="checkbox"].invalid + .checkbox-overlay {\n  border-color: red;\n}\n';

	function setupSettingsForm() {
	  settingsForm = $('<form>', {
	    id: 'settings',
	    style: 'display: none;',
	    on: {
	      submit: function submit(ev) {
	        ev.preventDefault();

	        var failed = false;

	        settingsForm.find('.setting').each(function (_, elem) {
	          elem = $(elem);
	          var key = elem.data('setting-key');
	          var setting = storage.get(key);
	          var value = setting.getInputValue(elem);
	          var success = setting.change(value);
	          setting.setValid(elem, success);

	          failed = failed || !success;
	        });

	        storage.saveSettings();

	        if (!failed) {
	          (function () {
	            var button = settingsForm.find('button');
	            button.text('Готово!');

	            setTimeout(function () {
	              settingsForm.slideUp(function () {
	                button.text('Сохранить');
	              });
	            }, 500);
	          })();
	        }
	      }
	    }
	  });

	  var _iteratorNormalCompletion3 = true;
	  var _didIteratorError3 = false;
	  var _iteratorError3 = undefined;

	  try {
	    for (var _iterator3 = (0, _getIterator3.default)(storage), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
	      var setting = _step3.value;

	      settingsForm.append(setting.render());
	    }
	  } catch (err) {
	    _didIteratorError3 = true;
	    _iteratorError3 = err;
	  } finally {
	    try {
	      if (!_iteratorNormalCompletion3 && _iterator3.return) {
	        _iterator3.return();
	      }
	    } finally {
	      if (_didIteratorError3) {
	        throw _iteratorError3;
	      }
	    }
	  }

	  var row = $('<div class="row"></div>');
	  $('<label>').appendTo(row);
	  $('<button>', {
	    'class': 'btn btn-default',
	    type: 'submit',
	    text: 'Сохранить'
	  }).appendTo(row);

	  settingsForm.append(row);

	  $('<style>', {
	    html: settingsFormStyle
	  }).appendTo(settingsForm);

	  $('#comments').append(settingsForm);
	}

	function loadSettings() {
	  storage.loadSettings();

	  autorefreshLeftSecs = autorefreshIntervalSecs.value;
	}

	var favicon = null;

	function getFavicon() {
	  if (favicon === null) {
	    favicon = $('head > link[rel~="icon"]');
	  }
	  return favicon;
	}

	function faviconNotify(flag) {
	  getFavicon().attr('href', flag ? _favicon.FAVICON_NOTIFY : _favicon.FAVICON_ORIGINAL);
	}

	function onDOMReady() {
	  loadSettings();
	  removePreviewsOnClick();
	  setShowPreviewCallback($('div.title > a'));
	  disableAutorefreshForClosedTopics();
	  setAutorefresher();
	  setupSettingsForm();
	  addResponsesLinks();
	}

	$(onDOMReady);

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = { "default": __webpack_require__(2), __esModule: true };

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(3);
	__webpack_require__(31);
	module.exports = __webpack_require__(34);

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(4);
	var Iterators = __webpack_require__(7);
	Iterators.NodeList = Iterators.HTMLCollection = Iterators.Array;

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	var addToUnscopables = __webpack_require__(5)
	  , step             = __webpack_require__(6)
	  , Iterators        = __webpack_require__(7)
	  , toIObject        = __webpack_require__(8);

	// 22.1.3.4 Array.prototype.entries()
	// 22.1.3.13 Array.prototype.keys()
	// 22.1.3.29 Array.prototype.values()
	// 22.1.3.30 Array.prototype[@@iterator]()
	module.exports = __webpack_require__(12)(Array, 'Array', function(iterated, kind){
	  this._t = toIObject(iterated); // target
	  this._i = 0;                   // next index
	  this._k = kind;                // kind
	// 22.1.5.2.1 %ArrayIteratorPrototype%.next()
	}, function(){
	  var O     = this._t
	    , kind  = this._k
	    , index = this._i++;
	  if(!O || index >= O.length){
	    this._t = undefined;
	    return step(1);
	  }
	  if(kind == 'keys'  )return step(0, index);
	  if(kind == 'values')return step(0, O[index]);
	  return step(0, [index, O[index]]);
	}, 'values');

	// argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
	Iterators.Arguments = Iterators.Array;

	addToUnscopables('keys');
	addToUnscopables('values');
	addToUnscopables('entries');

/***/ },
/* 5 */
/***/ function(module, exports) {

	module.exports = function(){ /* empty */ };

/***/ },
/* 6 */
/***/ function(module, exports) {

	module.exports = function(done, value){
	  return {value: value, done: !!done};
	};

/***/ },
/* 7 */
/***/ function(module, exports) {

	module.exports = {};

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	// to indexed object, toObject with fallback for non-array-like ES3 strings
	var IObject = __webpack_require__(9)
	  , defined = __webpack_require__(11);
	module.exports = function(it){
	  return IObject(defined(it));
	};

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	// fallback for non-array-like ES3 and non-enumerable old V8 strings
	var cof = __webpack_require__(10);
	module.exports = Object('z').propertyIsEnumerable(0) ? Object : function(it){
	  return cof(it) == 'String' ? it.split('') : Object(it);
	};

/***/ },
/* 10 */
/***/ function(module, exports) {

	var toString = {}.toString;

	module.exports = function(it){
	  return toString.call(it).slice(8, -1);
	};

/***/ },
/* 11 */
/***/ function(module, exports) {

	// 7.2.1 RequireObjectCoercible(argument)
	module.exports = function(it){
	  if(it == undefined)throw TypeError("Can't call method on  " + it);
	  return it;
	};

/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	var LIBRARY        = __webpack_require__(13)
	  , $export        = __webpack_require__(14)
	  , redefine       = __webpack_require__(19)
	  , hide           = __webpack_require__(20)
	  , has            = __webpack_require__(25)
	  , Iterators      = __webpack_require__(7)
	  , $iterCreate    = __webpack_require__(26)
	  , setToStringTag = __webpack_require__(27)
	  , getProto       = __webpack_require__(21).getProto
	  , ITERATOR       = __webpack_require__(28)('iterator')
	  , BUGGY          = !([].keys && 'next' in [].keys()) // Safari has buggy iterators w/o `next`
	  , FF_ITERATOR    = '@@iterator'
	  , KEYS           = 'keys'
	  , VALUES         = 'values';

	var returnThis = function(){ return this; };

	module.exports = function(Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED){
	  $iterCreate(Constructor, NAME, next);
	  var getMethod = function(kind){
	    if(!BUGGY && kind in proto)return proto[kind];
	    switch(kind){
	      case KEYS: return function keys(){ return new Constructor(this, kind); };
	      case VALUES: return function values(){ return new Constructor(this, kind); };
	    } return function entries(){ return new Constructor(this, kind); };
	  };
	  var TAG        = NAME + ' Iterator'
	    , DEF_VALUES = DEFAULT == VALUES
	    , VALUES_BUG = false
	    , proto      = Base.prototype
	    , $native    = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT]
	    , $default   = $native || getMethod(DEFAULT)
	    , methods, key;
	  // Fix native
	  if($native){
	    var IteratorPrototype = getProto($default.call(new Base));
	    // Set @@toStringTag to native iterators
	    setToStringTag(IteratorPrototype, TAG, true);
	    // FF fix
	    if(!LIBRARY && has(proto, FF_ITERATOR))hide(IteratorPrototype, ITERATOR, returnThis);
	    // fix Array#{values, @@iterator}.name in V8 / FF
	    if(DEF_VALUES && $native.name !== VALUES){
	      VALUES_BUG = true;
	      $default = function values(){ return $native.call(this); };
	    }
	  }
	  // Define iterator
	  if((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])){
	    hide(proto, ITERATOR, $default);
	  }
	  // Plug for library
	  Iterators[NAME] = $default;
	  Iterators[TAG]  = returnThis;
	  if(DEFAULT){
	    methods = {
	      values:  DEF_VALUES  ? $default : getMethod(VALUES),
	      keys:    IS_SET      ? $default : getMethod(KEYS),
	      entries: !DEF_VALUES ? $default : getMethod('entries')
	    };
	    if(FORCED)for(key in methods){
	      if(!(key in proto))redefine(proto, key, methods[key]);
	    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
	  }
	  return methods;
	};

/***/ },
/* 13 */
/***/ function(module, exports) {

	module.exports = true;

/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	var global    = __webpack_require__(15)
	  , core      = __webpack_require__(16)
	  , ctx       = __webpack_require__(17)
	  , PROTOTYPE = 'prototype';

	var $export = function(type, name, source){
	  var IS_FORCED = type & $export.F
	    , IS_GLOBAL = type & $export.G
	    , IS_STATIC = type & $export.S
	    , IS_PROTO  = type & $export.P
	    , IS_BIND   = type & $export.B
	    , IS_WRAP   = type & $export.W
	    , exports   = IS_GLOBAL ? core : core[name] || (core[name] = {})
	    , target    = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE]
	    , key, own, out;
	  if(IS_GLOBAL)source = name;
	  for(key in source){
	    // contains in native
	    own = !IS_FORCED && target && key in target;
	    if(own && key in exports)continue;
	    // export native or passed
	    out = own ? target[key] : source[key];
	    // prevent global pollution for namespaces
	    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
	    // bind timers to global for call from export context
	    : IS_BIND && own ? ctx(out, global)
	    // wrap global constructors for prevent change them in library
	    : IS_WRAP && target[key] == out ? (function(C){
	      var F = function(param){
	        return this instanceof C ? new C(param) : C(param);
	      };
	      F[PROTOTYPE] = C[PROTOTYPE];
	      return F;
	    // make static versions for prototype methods
	    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
	    if(IS_PROTO)(exports[PROTOTYPE] || (exports[PROTOTYPE] = {}))[key] = out;
	  }
	};
	// type bitmap
	$export.F = 1;  // forced
	$export.G = 2;  // global
	$export.S = 4;  // static
	$export.P = 8;  // proto
	$export.B = 16; // bind
	$export.W = 32; // wrap
	module.exports = $export;

/***/ },
/* 15 */
/***/ function(module, exports) {

	// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
	var global = module.exports = typeof window != 'undefined' && window.Math == Math
	  ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();
	if(typeof __g == 'number')__g = global; // eslint-disable-line no-undef

/***/ },
/* 16 */
/***/ function(module, exports) {

	var core = module.exports = {version: '1.2.6'};
	if(typeof __e == 'number')__e = core; // eslint-disable-line no-undef

/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

	// optional / simple context binding
	var aFunction = __webpack_require__(18);
	module.exports = function(fn, that, length){
	  aFunction(fn);
	  if(that === undefined)return fn;
	  switch(length){
	    case 1: return function(a){
	      return fn.call(that, a);
	    };
	    case 2: return function(a, b){
	      return fn.call(that, a, b);
	    };
	    case 3: return function(a, b, c){
	      return fn.call(that, a, b, c);
	    };
	  }
	  return function(/* ...args */){
	    return fn.apply(that, arguments);
	  };
	};

/***/ },
/* 18 */
/***/ function(module, exports) {

	module.exports = function(it){
	  if(typeof it != 'function')throw TypeError(it + ' is not a function!');
	  return it;
	};

/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(20);

/***/ },
/* 20 */
/***/ function(module, exports, __webpack_require__) {

	var $          = __webpack_require__(21)
	  , createDesc = __webpack_require__(22);
	module.exports = __webpack_require__(23) ? function(object, key, value){
	  return $.setDesc(object, key, createDesc(1, value));
	} : function(object, key, value){
	  object[key] = value;
	  return object;
	};

/***/ },
/* 21 */
/***/ function(module, exports) {

	var $Object = Object;
	module.exports = {
	  create:     $Object.create,
	  getProto:   $Object.getPrototypeOf,
	  isEnum:     {}.propertyIsEnumerable,
	  getDesc:    $Object.getOwnPropertyDescriptor,
	  setDesc:    $Object.defineProperty,
	  setDescs:   $Object.defineProperties,
	  getKeys:    $Object.keys,
	  getNames:   $Object.getOwnPropertyNames,
	  getSymbols: $Object.getOwnPropertySymbols,
	  each:       [].forEach
	};

/***/ },
/* 22 */
/***/ function(module, exports) {

	module.exports = function(bitmap, value){
	  return {
	    enumerable  : !(bitmap & 1),
	    configurable: !(bitmap & 2),
	    writable    : !(bitmap & 4),
	    value       : value
	  };
	};

/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

	// Thank's IE8 for his funny defineProperty
	module.exports = !__webpack_require__(24)(function(){
	  return Object.defineProperty({}, 'a', {get: function(){ return 7; }}).a != 7;
	});

/***/ },
/* 24 */
/***/ function(module, exports) {

	module.exports = function(exec){
	  try {
	    return !!exec();
	  } catch(e){
	    return true;
	  }
	};

/***/ },
/* 25 */
/***/ function(module, exports) {

	var hasOwnProperty = {}.hasOwnProperty;
	module.exports = function(it, key){
	  return hasOwnProperty.call(it, key);
	};

/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	var $              = __webpack_require__(21)
	  , descriptor     = __webpack_require__(22)
	  , setToStringTag = __webpack_require__(27)
	  , IteratorPrototype = {};

	// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
	__webpack_require__(20)(IteratorPrototype, __webpack_require__(28)('iterator'), function(){ return this; });

	module.exports = function(Constructor, NAME, next){
	  Constructor.prototype = $.create(IteratorPrototype, {next: descriptor(1, next)});
	  setToStringTag(Constructor, NAME + ' Iterator');
	};

/***/ },
/* 27 */
/***/ function(module, exports, __webpack_require__) {

	var def = __webpack_require__(21).setDesc
	  , has = __webpack_require__(25)
	  , TAG = __webpack_require__(28)('toStringTag');

	module.exports = function(it, tag, stat){
	  if(it && !has(it = stat ? it : it.prototype, TAG))def(it, TAG, {configurable: true, value: tag});
	};

/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

	var store  = __webpack_require__(29)('wks')
	  , uid    = __webpack_require__(30)
	  , Symbol = __webpack_require__(15).Symbol;
	module.exports = function(name){
	  return store[name] || (store[name] =
	    Symbol && Symbol[name] || (Symbol || uid)('Symbol.' + name));
	};

/***/ },
/* 29 */
/***/ function(module, exports, __webpack_require__) {

	var global = __webpack_require__(15)
	  , SHARED = '__core-js_shared__'
	  , store  = global[SHARED] || (global[SHARED] = {});
	module.exports = function(key){
	  return store[key] || (store[key] = {});
	};

/***/ },
/* 30 */
/***/ function(module, exports) {

	var id = 0
	  , px = Math.random();
	module.exports = function(key){
	  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
	};

/***/ },
/* 31 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	var $at  = __webpack_require__(32)(true);

	// 21.1.3.27 String.prototype[@@iterator]()
	__webpack_require__(12)(String, 'String', function(iterated){
	  this._t = String(iterated); // target
	  this._i = 0;                // next index
	// 21.1.5.2.1 %StringIteratorPrototype%.next()
	}, function(){
	  var O     = this._t
	    , index = this._i
	    , point;
	  if(index >= O.length)return {value: undefined, done: true};
	  point = $at(O, index);
	  this._i += point.length;
	  return {value: point, done: false};
	});

/***/ },
/* 32 */
/***/ function(module, exports, __webpack_require__) {

	var toInteger = __webpack_require__(33)
	  , defined   = __webpack_require__(11);
	// true  -> String#at
	// false -> String#codePointAt
	module.exports = function(TO_STRING){
	  return function(that, pos){
	    var s = String(defined(that))
	      , i = toInteger(pos)
	      , l = s.length
	      , a, b;
	    if(i < 0 || i >= l)return TO_STRING ? '' : undefined;
	    a = s.charCodeAt(i);
	    return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff
	      ? TO_STRING ? s.charAt(i) : a
	      : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
	  };
	};

/***/ },
/* 33 */
/***/ function(module, exports) {

	// 7.1.4 ToInteger
	var ceil  = Math.ceil
	  , floor = Math.floor;
	module.exports = function(it){
	  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
	};

/***/ },
/* 34 */
/***/ function(module, exports, __webpack_require__) {

	var anObject = __webpack_require__(35)
	  , get      = __webpack_require__(37);
	module.exports = __webpack_require__(16).getIterator = function(it){
	  var iterFn = get(it);
	  if(typeof iterFn != 'function')throw TypeError(it + ' is not iterable!');
	  return anObject(iterFn.call(it));
	};

/***/ },
/* 35 */
/***/ function(module, exports, __webpack_require__) {

	var isObject = __webpack_require__(36);
	module.exports = function(it){
	  if(!isObject(it))throw TypeError(it + ' is not an object!');
	  return it;
	};

/***/ },
/* 36 */
/***/ function(module, exports) {

	module.exports = function(it){
	  return typeof it === 'object' ? it !== null : typeof it === 'function';
	};

/***/ },
/* 37 */
/***/ function(module, exports, __webpack_require__) {

	var classof   = __webpack_require__(38)
	  , ITERATOR  = __webpack_require__(28)('iterator')
	  , Iterators = __webpack_require__(7);
	module.exports = __webpack_require__(16).getIteratorMethod = function(it){
	  if(it != undefined)return it[ITERATOR]
	    || it['@@iterator']
	    || Iterators[classof(it)];
	};

/***/ },
/* 38 */
/***/ function(module, exports, __webpack_require__) {

	// getting tag from 19.1.3.6 Object.prototype.toString()
	var cof = __webpack_require__(10)
	  , TAG = __webpack_require__(28)('toStringTag')
	  // ES3 wrong here
	  , ARG = cof(function(){ return arguments; }()) == 'Arguments';

	module.exports = function(it){
	  var O, T, B;
	  return it === undefined ? 'Undefined' : it === null ? 'Null'
	    // @@toStringTag case
	    : typeof (T = (O = Object(it))[TAG]) == 'string' ? T
	    // builtinTag case
	    : ARG ? cof(O)
	    // ES3 arguments fallback
	    : (B = cof(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : B;
	};

/***/ },
/* 39 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	/**
	 * @external {jQuery} https://api.jquery.com/Types/#jQuery
	 */

	/**
	 * Class representing a thread comment.
	 */

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.CommentCache = exports.Comment = undefined;

	var _createClass2 = __webpack_require__(40);

	var _createClass3 = _interopRequireDefault(_createClass2);

	var _classCallCheck2 = __webpack_require__(43);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var Comment =
	/**
	 * Create a new {@link Comment}.
	 *
	 * @param {jQuery} element - jQuery wrapper around the comment's DOM element
	 * @param {string} id - comment's ID
	 * @param {string} link - comment's URL
	 * @param {?string} referencedCommentID - ID of the comment's parent, if any
	 * @param {?string} author - author of the comment, if any
	 */
	exports.Comment = function Comment(element, id, link, referencedCommentID, author) {
	  (0, _classCallCheck3.default)(this, Comment);

	  /**
	   * jQuery wrapper around the comment's DOM element.
	   * @type {jQuery}
	   */
	  this.element = element;

	  /**
	   * Comment's ID.
	   * @type {string}
	   */
	  this.id = id;

	  /**
	   * Comment's URL.
	   * @type {string}
	   */
	  this.link = link;

	  /**
	   * ID of the comment's parent, if any.
	   * @type {?string}
	   */
	  this.referencedCommentID = referencedCommentID;

	  /**
	   * Author of the comment, if any.
	   * @type {?string}
	   */
	  this.author = author;
	};

	/**
	 * A cache which stores comments by {@link Comment#id}
	 */


	var CommentCache = exports.CommentCache = function () {
	  /**
	   * Create a new empty cache.
	   */

	  function CommentCache() {
	    (0, _classCallCheck3.default)(this, CommentCache);

	    this._cache = {};
	  }

	  /**
	   * Add a comment to the cache.
	   *
	   * @param {Comment} comment
	   */


	  (0, _createClass3.default)(CommentCache, [{
	    key: 'add',
	    value: function add(comment) {
	      this._cache[comment.id] = comment;
	    }

	    /**
	     * Find a comment by ID
	     *
	     * @param {string} id
	     */

	  }, {
	    key: 'find',
	    value: function find(id) {
	      return this._cache[id] || null;
	    }
	  }]);
	  return CommentCache;
	}();

/***/ },
/* 40 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";

	exports.__esModule = true;

	var _defineProperty = __webpack_require__(41);

	var _defineProperty2 = _interopRequireDefault(_defineProperty);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = (function () {
	  function defineProperties(target, props) {
	    for (var i = 0; i < props.length; i++) {
	      var descriptor = props[i];
	      descriptor.enumerable = descriptor.enumerable || false;
	      descriptor.configurable = true;
	      if ("value" in descriptor) descriptor.writable = true;
	      (0, _defineProperty2.default)(target, descriptor.key, descriptor);
	    }
	  }

	  return function (Constructor, protoProps, staticProps) {
	    if (protoProps) defineProperties(Constructor.prototype, protoProps);
	    if (staticProps) defineProperties(Constructor, staticProps);
	    return Constructor;
	  };
	})();

/***/ },
/* 41 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = { "default": __webpack_require__(42), __esModule: true };

/***/ },
/* 42 */
/***/ function(module, exports, __webpack_require__) {

	var $ = __webpack_require__(21);
	module.exports = function defineProperty(it, key, desc){
	  return $.setDesc(it, key, desc);
	};

/***/ },
/* 43 */
/***/ function(module, exports) {

	"use strict";

	exports.__esModule = true;

	exports.default = function (instance, Constructor) {
	  if (!(instance instanceof Constructor)) {
	    throw new TypeError("Cannot call a class as a function");
	  }
	};

/***/ },
/* 44 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	/**
	 * An abstract storage class.
	 *
	 * It keeps track of all registered settings by {@link Setting#key}.
	 */

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.LocalStorage = exports.Storage = undefined;

	var _getPrototypeOf = __webpack_require__(45);

	var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

	var _possibleConstructorReturn2 = __webpack_require__(50);

	var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

	var _inherits2 = __webpack_require__(60);

	var _inherits3 = _interopRequireDefault(_inherits2);

	var _regenerator = __webpack_require__(67);

	var _regenerator2 = _interopRequireDefault(_regenerator);

	var _iterator3 = __webpack_require__(88);

	var _iterator4 = _interopRequireDefault(_iterator3);

	var _getIterator2 = __webpack_require__(1);

	var _getIterator3 = _interopRequireDefault(_getIterator2);

	var _classCallCheck2 = __webpack_require__(43);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(40);

	var _createClass3 = _interopRequireDefault(_createClass2);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var Storage = exports.Storage = function () {
	  /**
	   * Create a new storage.
	   *
	   * This method MUST be called in every subclass that overrides the default constructor.
	   *
	   * @example
	   *
	   * class MyStorage extends Storage {
	   *   // Custom constructor:
	   *   constructor() {
	   *     super() // Call the Storage's constructor
	   *   }
	   * }
	   */

	  function Storage() {
	    (0, _classCallCheck3.default)(this, Storage);

	    if (new.target === Storage) {
	      throw new TypeError("Cannot directly create an instance of Storage.");
	    }

	    this._settings = {};
	  }

	  /**
	   * Add a new {@link Setting} to the storage's settings registry.
	   *
	   * @param {Setting} setting
	   */


	  (0, _createClass3.default)(Storage, [{
	    key: 'registerSetting',
	    value: function registerSetting(setting) {
	      this._settings[setting.key] = setting;
	    }

	    /**
	     * Load all registered settings from the storage.
	     */

	  }, {
	    key: 'loadSettings',
	    value: function loadSettings() {
	      var _iteratorNormalCompletion = true;
	      var _didIteratorError = false;
	      var _iteratorError = undefined;

	      try {
	        for (var _iterator = (0, _getIterator3.default)(this), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	          var setting = _step.value;

	          setting.load();
	        }
	      } catch (err) {
	        _didIteratorError = true;
	        _iteratorError = err;
	      } finally {
	        try {
	          if (!_iteratorNormalCompletion && _iterator.return) {
	            _iterator.return();
	          }
	        } finally {
	          if (_didIteratorError) {
	            throw _iteratorError;
	          }
	        }
	      }
	    }

	    /**
	     * Save all registered settings to the storage.
	     */

	  }, {
	    key: 'saveSettings',
	    value: function saveSettings() {
	      var _iteratorNormalCompletion2 = true;
	      var _didIteratorError2 = false;
	      var _iteratorError2 = undefined;

	      try {
	        for (var _iterator2 = (0, _getIterator3.default)(this), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
	          var setting = _step2.value;

	          setting.save();
	        }
	      } catch (err) {
	        _didIteratorError2 = true;
	        _iteratorError2 = err;
	      } finally {
	        try {
	          if (!_iteratorNormalCompletion2 && _iterator2.return) {
	            _iterator2.return();
	          }
	        } finally {
	          if (_didIteratorError2) {
	            throw _iteratorError2;
	          }
	        }
	      }
	    }

	    /**
	     * Get a {@link Setting} by key.
	     *
	     * @param {string} key
	     * @return {Setting}
	     */

	  }, {
	    key: 'get',
	    value: function get(key) {
	      return this._settings[key];
	    }

	    /**
	     * Return a generator to iterate over all registered settings.
	     *
	     * @example
	     * for (let setting of storage) {
	     *   ...
	     * }
	     */

	  }, {
	    key: _iterator4.default,
	    value: _regenerator2.default.mark(function value() {
	      var key;
	      return _regenerator2.default.wrap(function value$(_context) {
	        while (1) {
	          switch (_context.prev = _context.next) {
	            case 0:
	              _context.t0 = _regenerator2.default.keys(this._settings);

	            case 1:
	              if ((_context.t1 = _context.t0()).done) {
	                _context.next = 7;
	                break;
	              }

	              key = _context.t1.value;
	              _context.next = 5;
	              return this._settings[key];

	            case 5:
	              _context.next = 1;
	              break;

	            case 7:
	            case 'end':
	              return _context.stop();
	          }
	        }
	      }, value, this);
	    })

	    /**
	     * Store the setting in the storage.
	     *
	     * @abstract
	     * @param {Setting} setting
	     */

	  }, {
	    key: 'store',
	    value: function store(setting) {
	      setting = setting; // Ignore eslint's complains about unused variable
	      throw new Error('unimplemented');
	    }

	    /**
	     * Return the value of previously stored setting.
	     *
	     * @abstract
	     * @param {Setting} setting
	     * @return {*} previously stored value or null, if none
	     */

	  }, {
	    key: 'load',
	    value: function load(setting) {
	      setting = setting; // Ignore eslint's complains about unused variable
	      throw new Error('unimplemented');
	    }
	  }]);
	  return Storage;
	}();

	/**
	 * Persist settings via browser's Local Storage.
	 */


	var LocalStorage = exports.LocalStorage = function (_Storage) {
	  (0, _inherits3.default)(LocalStorage, _Storage);

	  function LocalStorage() {
	    (0, _classCallCheck3.default)(this, LocalStorage);
	    return (0, _possibleConstructorReturn3.default)(this, (0, _getPrototypeOf2.default)(LocalStorage).apply(this, arguments));
	  }

	  (0, _createClass3.default)(LocalStorage, [{
	    key: 'store',


	    /**
	     * Store the setting in Local Storage.
	     *
	     * @override
	     * @param {Setting} setting
	     */
	    value: function store(setting) {
	      if (LocalStorage.available) {
	        localStorage.setItem(setting.key, setting.value);
	      }
	    }

	    /**
	     * Return the value of previously stored setting.
	     *
	     * @override
	     * @param {Setting} setting
	     * @return {*} previously stored value or null, if none
	     */

	  }, {
	    key: 'load',
	    value: function load(setting) {
	      if (LocalStorage.available) {
	        return localStorage.getItem(setting.key);
	      }

	      return null;
	    }
	  }], [{
	    key: 'available',

	    /**
	     * Indicate whether the browser supports local storage.
	     * @see https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API/Using_the_Web_Storage_API#Testing_for_support_vs_availability
	     */
	    get: function get() {
	      try {
	        var storage = window.localStorage;
	        var x = '__storage_test__';

	        storage.setItem(x, x);
	        storage.removeItem(x);

	        return true;
	      } catch (e) {
	        return false;
	      }
	    }
	  }]);
	  return LocalStorage;
	}(Storage);

/***/ },
/* 45 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = { "default": __webpack_require__(46), __esModule: true };

/***/ },
/* 46 */
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(47);
	module.exports = __webpack_require__(16).Object.getPrototypeOf;

/***/ },
/* 47 */
/***/ function(module, exports, __webpack_require__) {

	// 19.1.2.9 Object.getPrototypeOf(O)
	var toObject = __webpack_require__(48);

	__webpack_require__(49)('getPrototypeOf', function($getPrototypeOf){
	  return function getPrototypeOf(it){
	    return $getPrototypeOf(toObject(it));
	  };
	});

/***/ },
/* 48 */
/***/ function(module, exports, __webpack_require__) {

	// 7.1.13 ToObject(argument)
	var defined = __webpack_require__(11);
	module.exports = function(it){
	  return Object(defined(it));
	};

/***/ },
/* 49 */
/***/ function(module, exports, __webpack_require__) {

	// most Object methods by ES6 should accept primitives
	var $export = __webpack_require__(14)
	  , core    = __webpack_require__(16)
	  , fails   = __webpack_require__(24);
	module.exports = function(KEY, exec){
	  var fn  = (core.Object || {})[KEY] || Object[KEY]
	    , exp = {};
	  exp[KEY] = exec(fn);
	  $export($export.S + $export.F * fails(function(){ fn(1); }), 'Object', exp);
	};

/***/ },
/* 50 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";

	exports.__esModule = true;

	var _typeof2 = __webpack_require__(51);

	var _typeof3 = _interopRequireDefault(_typeof2);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = function (self, call) {
	  if (!self) {
	    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	  }

	  return call && ((typeof call === "undefined" ? "undefined" : (0, _typeof3.default)(call)) === "object" || typeof call === "function") ? call : self;
	};

/***/ },
/* 51 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";

	var _Symbol = __webpack_require__(52)["default"];

	exports["default"] = function (obj) {
	  return obj && obj.constructor === _Symbol ? "symbol" : typeof obj;
	};

	exports.__esModule = true;

/***/ },
/* 52 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = { "default": __webpack_require__(53), __esModule: true };

/***/ },
/* 53 */
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(54);
	__webpack_require__(59);
	module.exports = __webpack_require__(16).Symbol;

/***/ },
/* 54 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	// ECMAScript 6 symbols shim
	var $              = __webpack_require__(21)
	  , global         = __webpack_require__(15)
	  , has            = __webpack_require__(25)
	  , DESCRIPTORS    = __webpack_require__(23)
	  , $export        = __webpack_require__(14)
	  , redefine       = __webpack_require__(19)
	  , $fails         = __webpack_require__(24)
	  , shared         = __webpack_require__(29)
	  , setToStringTag = __webpack_require__(27)
	  , uid            = __webpack_require__(30)
	  , wks            = __webpack_require__(28)
	  , keyOf          = __webpack_require__(55)
	  , $names         = __webpack_require__(56)
	  , enumKeys       = __webpack_require__(57)
	  , isArray        = __webpack_require__(58)
	  , anObject       = __webpack_require__(35)
	  , toIObject      = __webpack_require__(8)
	  , createDesc     = __webpack_require__(22)
	  , getDesc        = $.getDesc
	  , setDesc        = $.setDesc
	  , _create        = $.create
	  , getNames       = $names.get
	  , $Symbol        = global.Symbol
	  , $JSON          = global.JSON
	  , _stringify     = $JSON && $JSON.stringify
	  , setter         = false
	  , HIDDEN         = wks('_hidden')
	  , isEnum         = $.isEnum
	  , SymbolRegistry = shared('symbol-registry')
	  , AllSymbols     = shared('symbols')
	  , useNative      = typeof $Symbol == 'function'
	  , ObjectProto    = Object.prototype;

	// fallback for old Android, https://code.google.com/p/v8/issues/detail?id=687
	var setSymbolDesc = DESCRIPTORS && $fails(function(){
	  return _create(setDesc({}, 'a', {
	    get: function(){ return setDesc(this, 'a', {value: 7}).a; }
	  })).a != 7;
	}) ? function(it, key, D){
	  var protoDesc = getDesc(ObjectProto, key);
	  if(protoDesc)delete ObjectProto[key];
	  setDesc(it, key, D);
	  if(protoDesc && it !== ObjectProto)setDesc(ObjectProto, key, protoDesc);
	} : setDesc;

	var wrap = function(tag){
	  var sym = AllSymbols[tag] = _create($Symbol.prototype);
	  sym._k = tag;
	  DESCRIPTORS && setter && setSymbolDesc(ObjectProto, tag, {
	    configurable: true,
	    set: function(value){
	      if(has(this, HIDDEN) && has(this[HIDDEN], tag))this[HIDDEN][tag] = false;
	      setSymbolDesc(this, tag, createDesc(1, value));
	    }
	  });
	  return sym;
	};

	var isSymbol = function(it){
	  return typeof it == 'symbol';
	};

	var $defineProperty = function defineProperty(it, key, D){
	  if(D && has(AllSymbols, key)){
	    if(!D.enumerable){
	      if(!has(it, HIDDEN))setDesc(it, HIDDEN, createDesc(1, {}));
	      it[HIDDEN][key] = true;
	    } else {
	      if(has(it, HIDDEN) && it[HIDDEN][key])it[HIDDEN][key] = false;
	      D = _create(D, {enumerable: createDesc(0, false)});
	    } return setSymbolDesc(it, key, D);
	  } return setDesc(it, key, D);
	};
	var $defineProperties = function defineProperties(it, P){
	  anObject(it);
	  var keys = enumKeys(P = toIObject(P))
	    , i    = 0
	    , l = keys.length
	    , key;
	  while(l > i)$defineProperty(it, key = keys[i++], P[key]);
	  return it;
	};
	var $create = function create(it, P){
	  return P === undefined ? _create(it) : $defineProperties(_create(it), P);
	};
	var $propertyIsEnumerable = function propertyIsEnumerable(key){
	  var E = isEnum.call(this, key);
	  return E || !has(this, key) || !has(AllSymbols, key) || has(this, HIDDEN) && this[HIDDEN][key]
	    ? E : true;
	};
	var $getOwnPropertyDescriptor = function getOwnPropertyDescriptor(it, key){
	  var D = getDesc(it = toIObject(it), key);
	  if(D && has(AllSymbols, key) && !(has(it, HIDDEN) && it[HIDDEN][key]))D.enumerable = true;
	  return D;
	};
	var $getOwnPropertyNames = function getOwnPropertyNames(it){
	  var names  = getNames(toIObject(it))
	    , result = []
	    , i      = 0
	    , key;
	  while(names.length > i)if(!has(AllSymbols, key = names[i++]) && key != HIDDEN)result.push(key);
	  return result;
	};
	var $getOwnPropertySymbols = function getOwnPropertySymbols(it){
	  var names  = getNames(toIObject(it))
	    , result = []
	    , i      = 0
	    , key;
	  while(names.length > i)if(has(AllSymbols, key = names[i++]))result.push(AllSymbols[key]);
	  return result;
	};
	var $stringify = function stringify(it){
	  if(it === undefined || isSymbol(it))return; // IE8 returns string on undefined
	  var args = [it]
	    , i    = 1
	    , $$   = arguments
	    , replacer, $replacer;
	  while($$.length > i)args.push($$[i++]);
	  replacer = args[1];
	  if(typeof replacer == 'function')$replacer = replacer;
	  if($replacer || !isArray(replacer))replacer = function(key, value){
	    if($replacer)value = $replacer.call(this, key, value);
	    if(!isSymbol(value))return value;
	  };
	  args[1] = replacer;
	  return _stringify.apply($JSON, args);
	};
	var buggyJSON = $fails(function(){
	  var S = $Symbol();
	  // MS Edge converts symbol values to JSON as {}
	  // WebKit converts symbol values to JSON as null
	  // V8 throws on boxed symbols
	  return _stringify([S]) != '[null]' || _stringify({a: S}) != '{}' || _stringify(Object(S)) != '{}';
	});

	// 19.4.1.1 Symbol([description])
	if(!useNative){
	  $Symbol = function Symbol(){
	    if(isSymbol(this))throw TypeError('Symbol is not a constructor');
	    return wrap(uid(arguments.length > 0 ? arguments[0] : undefined));
	  };
	  redefine($Symbol.prototype, 'toString', function toString(){
	    return this._k;
	  });

	  isSymbol = function(it){
	    return it instanceof $Symbol;
	  };

	  $.create     = $create;
	  $.isEnum     = $propertyIsEnumerable;
	  $.getDesc    = $getOwnPropertyDescriptor;
	  $.setDesc    = $defineProperty;
	  $.setDescs   = $defineProperties;
	  $.getNames   = $names.get = $getOwnPropertyNames;
	  $.getSymbols = $getOwnPropertySymbols;

	  if(DESCRIPTORS && !__webpack_require__(13)){
	    redefine(ObjectProto, 'propertyIsEnumerable', $propertyIsEnumerable, true);
	  }
	}

	var symbolStatics = {
	  // 19.4.2.1 Symbol.for(key)
	  'for': function(key){
	    return has(SymbolRegistry, key += '')
	      ? SymbolRegistry[key]
	      : SymbolRegistry[key] = $Symbol(key);
	  },
	  // 19.4.2.5 Symbol.keyFor(sym)
	  keyFor: function keyFor(key){
	    return keyOf(SymbolRegistry, key);
	  },
	  useSetter: function(){ setter = true; },
	  useSimple: function(){ setter = false; }
	};
	// 19.4.2.2 Symbol.hasInstance
	// 19.4.2.3 Symbol.isConcatSpreadable
	// 19.4.2.4 Symbol.iterator
	// 19.4.2.6 Symbol.match
	// 19.4.2.8 Symbol.replace
	// 19.4.2.9 Symbol.search
	// 19.4.2.10 Symbol.species
	// 19.4.2.11 Symbol.split
	// 19.4.2.12 Symbol.toPrimitive
	// 19.4.2.13 Symbol.toStringTag
	// 19.4.2.14 Symbol.unscopables
	$.each.call((
	  'hasInstance,isConcatSpreadable,iterator,match,replace,search,' +
	  'species,split,toPrimitive,toStringTag,unscopables'
	).split(','), function(it){
	  var sym = wks(it);
	  symbolStatics[it] = useNative ? sym : wrap(sym);
	});

	setter = true;

	$export($export.G + $export.W, {Symbol: $Symbol});

	$export($export.S, 'Symbol', symbolStatics);

	$export($export.S + $export.F * !useNative, 'Object', {
	  // 19.1.2.2 Object.create(O [, Properties])
	  create: $create,
	  // 19.1.2.4 Object.defineProperty(O, P, Attributes)
	  defineProperty: $defineProperty,
	  // 19.1.2.3 Object.defineProperties(O, Properties)
	  defineProperties: $defineProperties,
	  // 19.1.2.6 Object.getOwnPropertyDescriptor(O, P)
	  getOwnPropertyDescriptor: $getOwnPropertyDescriptor,
	  // 19.1.2.7 Object.getOwnPropertyNames(O)
	  getOwnPropertyNames: $getOwnPropertyNames,
	  // 19.1.2.8 Object.getOwnPropertySymbols(O)
	  getOwnPropertySymbols: $getOwnPropertySymbols
	});

	// 24.3.2 JSON.stringify(value [, replacer [, space]])
	$JSON && $export($export.S + $export.F * (!useNative || buggyJSON), 'JSON', {stringify: $stringify});

	// 19.4.3.5 Symbol.prototype[@@toStringTag]
	setToStringTag($Symbol, 'Symbol');
	// 20.2.1.9 Math[@@toStringTag]
	setToStringTag(Math, 'Math', true);
	// 24.3.3 JSON[@@toStringTag]
	setToStringTag(global.JSON, 'JSON', true);

/***/ },
/* 55 */
/***/ function(module, exports, __webpack_require__) {

	var $         = __webpack_require__(21)
	  , toIObject = __webpack_require__(8);
	module.exports = function(object, el){
	  var O      = toIObject(object)
	    , keys   = $.getKeys(O)
	    , length = keys.length
	    , index  = 0
	    , key;
	  while(length > index)if(O[key = keys[index++]] === el)return key;
	};

/***/ },
/* 56 */
/***/ function(module, exports, __webpack_require__) {

	// fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window
	var toIObject = __webpack_require__(8)
	  , getNames  = __webpack_require__(21).getNames
	  , toString  = {}.toString;

	var windowNames = typeof window == 'object' && Object.getOwnPropertyNames
	  ? Object.getOwnPropertyNames(window) : [];

	var getWindowNames = function(it){
	  try {
	    return getNames(it);
	  } catch(e){
	    return windowNames.slice();
	  }
	};

	module.exports.get = function getOwnPropertyNames(it){
	  if(windowNames && toString.call(it) == '[object Window]')return getWindowNames(it);
	  return getNames(toIObject(it));
	};

/***/ },
/* 57 */
/***/ function(module, exports, __webpack_require__) {

	// all enumerable object keys, includes symbols
	var $ = __webpack_require__(21);
	module.exports = function(it){
	  var keys       = $.getKeys(it)
	    , getSymbols = $.getSymbols;
	  if(getSymbols){
	    var symbols = getSymbols(it)
	      , isEnum  = $.isEnum
	      , i       = 0
	      , key;
	    while(symbols.length > i)if(isEnum.call(it, key = symbols[i++]))keys.push(key);
	  }
	  return keys;
	};

/***/ },
/* 58 */
/***/ function(module, exports, __webpack_require__) {

	// 7.2.2 IsArray(argument)
	var cof = __webpack_require__(10);
	module.exports = Array.isArray || function(arg){
	  return cof(arg) == 'Array';
	};

/***/ },
/* 59 */
/***/ function(module, exports) {

	

/***/ },
/* 60 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";

	var _Object$create = __webpack_require__(61)["default"];

	var _Object$setPrototypeOf = __webpack_require__(63)["default"];

	exports["default"] = function (subClass, superClass) {
	  if (typeof superClass !== "function" && superClass !== null) {
	    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	  }

	  subClass.prototype = _Object$create(superClass && superClass.prototype, {
	    constructor: {
	      value: subClass,
	      enumerable: false,
	      writable: true,
	      configurable: true
	    }
	  });
	  if (superClass) _Object$setPrototypeOf ? _Object$setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
	};

	exports.__esModule = true;

/***/ },
/* 61 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = { "default": __webpack_require__(62), __esModule: true };

/***/ },
/* 62 */
/***/ function(module, exports, __webpack_require__) {

	var $ = __webpack_require__(21);
	module.exports = function create(P, D){
	  return $.create(P, D);
	};

/***/ },
/* 63 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = { "default": __webpack_require__(64), __esModule: true };

/***/ },
/* 64 */
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(65);
	module.exports = __webpack_require__(16).Object.setPrototypeOf;

/***/ },
/* 65 */
/***/ function(module, exports, __webpack_require__) {

	// 19.1.3.19 Object.setPrototypeOf(O, proto)
	var $export = __webpack_require__(14);
	$export($export.S, 'Object', {setPrototypeOf: __webpack_require__(66).set});

/***/ },
/* 66 */
/***/ function(module, exports, __webpack_require__) {

	// Works with __proto__ only. Old v8 can't work with null proto objects.
	/* eslint-disable no-proto */
	var getDesc  = __webpack_require__(21).getDesc
	  , isObject = __webpack_require__(36)
	  , anObject = __webpack_require__(35);
	var check = function(O, proto){
	  anObject(O);
	  if(!isObject(proto) && proto !== null)throw TypeError(proto + ": can't set as prototype!");
	};
	module.exports = {
	  set: Object.setPrototypeOf || ('__proto__' in {} ? // eslint-disable-line
	    function(test, buggy, set){
	      try {
	        set = __webpack_require__(17)(Function.call, getDesc(Object.prototype, '__proto__').set, 2);
	        set(test, []);
	        buggy = !(test instanceof Array);
	      } catch(e){ buggy = true; }
	      return function setPrototypeOf(O, proto){
	        check(O, proto);
	        if(buggy)O.__proto__ = proto;
	        else set(O, proto);
	        return O;
	      };
	    }({}, false) : undefined),
	  check: check
	};

/***/ },
/* 67 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {// This method of obtaining a reference to the global object needs to be
	// kept identical to the way it is obtained in runtime.js
	var g =
	  typeof global === "object" ? global :
	  typeof window === "object" ? window :
	  typeof self === "object" ? self : this;

	// Use `getOwnPropertyNames` because not all browsers support calling
	// `hasOwnProperty` on the global `self` object in a worker. See #183.
	var hadRuntime = g.regeneratorRuntime &&
	  Object.getOwnPropertyNames(g).indexOf("regeneratorRuntime") >= 0;

	// Save the old regeneratorRuntime in case it needs to be restored later.
	var oldRuntime = hadRuntime && g.regeneratorRuntime;

	// Force reevalutation of runtime.js.
	g.regeneratorRuntime = undefined;

	module.exports = __webpack_require__(68);

	if (hadRuntime) {
	  // Restore the original runtime.
	  g.regeneratorRuntime = oldRuntime;
	} else {
	  // Remove the global property added by runtime.js.
	  try {
	    delete g.regeneratorRuntime;
	  } catch(e) {
	    g.regeneratorRuntime = undefined;
	  }
	}

	module.exports = { "default": module.exports, __esModule: true };

	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 68 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global, process) {/**
	 * Copyright (c) 2014, Facebook, Inc.
	 * All rights reserved.
	 *
	 * This source code is licensed under the BSD-style license found in the
	 * https://raw.github.com/facebook/regenerator/master/LICENSE file. An
	 * additional grant of patent rights can be found in the PATENTS file in
	 * the same directory.
	 */

	"use strict";

	var _Symbol = __webpack_require__(52)["default"];

	var _Object$create = __webpack_require__(61)["default"];

	var _Object$setPrototypeOf = __webpack_require__(63)["default"];

	var _Promise = __webpack_require__(70)["default"];

	!(function (global) {
	  "use strict";

	  var hasOwn = Object.prototype.hasOwnProperty;
	  var undefined; // More compressible than void 0.
	  var $Symbol = typeof _Symbol === "function" ? _Symbol : {};
	  var iteratorSymbol = $Symbol.iterator || "@@iterator";
	  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

	  var inModule = typeof module === "object";
	  var runtime = global.regeneratorRuntime;
	  if (runtime) {
	    if (inModule) {
	      // If regeneratorRuntime is defined globally and we're in a module,
	      // make the exports object identical to regeneratorRuntime.
	      module.exports = runtime;
	    }
	    // Don't bother evaluating the rest of this file if the runtime was
	    // already defined globally.
	    return;
	  }

	  // Define the runtime globally (as expected by generated code) as either
	  // module.exports (if we're in a module) or a new, empty object.
	  runtime = global.regeneratorRuntime = inModule ? module.exports : {};

	  function wrap(innerFn, outerFn, self, tryLocsList) {
	    // If outerFn provided, then outerFn.prototype instanceof Generator.
	    var generator = _Object$create((outerFn || Generator).prototype);
	    var context = new Context(tryLocsList || []);

	    // The ._invoke method unifies the implementations of the .next,
	    // .throw, and .return methods.
	    generator._invoke = makeInvokeMethod(innerFn, self, context);

	    return generator;
	  }
	  runtime.wrap = wrap;

	  // Try/catch helper to minimize deoptimizations. Returns a completion
	  // record like context.tryEntries[i].completion. This interface could
	  // have been (and was previously) designed to take a closure to be
	  // invoked without arguments, but in all the cases we care about we
	  // already have an existing method we want to call, so there's no need
	  // to create a new function object. We can even get away with assuming
	  // the method takes exactly one argument, since that happens to be true
	  // in every case, so we don't have to touch the arguments object. The
	  // only additional allocation required is the completion record, which
	  // has a stable shape and so hopefully should be cheap to allocate.
	  function tryCatch(fn, obj, arg) {
	    try {
	      return { type: "normal", arg: fn.call(obj, arg) };
	    } catch (err) {
	      return { type: "throw", arg: err };
	    }
	  }

	  var GenStateSuspendedStart = "suspendedStart";
	  var GenStateSuspendedYield = "suspendedYield";
	  var GenStateExecuting = "executing";
	  var GenStateCompleted = "completed";

	  // Returning this object from the innerFn has the same effect as
	  // breaking out of the dispatch switch statement.
	  var ContinueSentinel = {};

	  // Dummy constructor functions that we use as the .constructor and
	  // .constructor.prototype properties for functions that return Generator
	  // objects. For full spec compliance, you may wish to configure your
	  // minifier not to mangle the names of these two functions.
	  function Generator() {}
	  function GeneratorFunction() {}
	  function GeneratorFunctionPrototype() {}

	  var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype;
	  GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
	  GeneratorFunctionPrototype.constructor = GeneratorFunction;
	  GeneratorFunctionPrototype[toStringTagSymbol] = GeneratorFunction.displayName = "GeneratorFunction";

	  // Helper for defining the .next, .throw, and .return methods of the
	  // Iterator interface in terms of a single ._invoke method.
	  function defineIteratorMethods(prototype) {
	    ["next", "throw", "return"].forEach(function (method) {
	      prototype[method] = function (arg) {
	        return this._invoke(method, arg);
	      };
	    });
	  }

	  runtime.isGeneratorFunction = function (genFun) {
	    var ctor = typeof genFun === "function" && genFun.constructor;
	    return ctor ? ctor === GeneratorFunction ||
	    // For the native GeneratorFunction constructor, the best we can
	    // do is to check its .name property.
	    (ctor.displayName || ctor.name) === "GeneratorFunction" : false;
	  };

	  runtime.mark = function (genFun) {
	    if (_Object$setPrototypeOf) {
	      _Object$setPrototypeOf(genFun, GeneratorFunctionPrototype);
	    } else {
	      genFun.__proto__ = GeneratorFunctionPrototype;
	      if (!(toStringTagSymbol in genFun)) {
	        genFun[toStringTagSymbol] = "GeneratorFunction";
	      }
	    }
	    genFun.prototype = _Object$create(Gp);
	    return genFun;
	  };

	  // Within the body of any async function, `await x` is transformed to
	  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
	  // `value instanceof AwaitArgument` to determine if the yielded value is
	  // meant to be awaited. Some may consider the name of this method too
	  // cutesy, but they are curmudgeons.
	  runtime.awrap = function (arg) {
	    return new AwaitArgument(arg);
	  };

	  function AwaitArgument(arg) {
	    this.arg = arg;
	  }

	  function AsyncIterator(generator) {
	    function invoke(method, arg, resolve, reject) {
	      var record = tryCatch(generator[method], generator, arg);
	      if (record.type === "throw") {
	        reject(record.arg);
	      } else {
	        var result = record.arg;
	        var value = result.value;
	        if (value instanceof AwaitArgument) {
	          return _Promise.resolve(value.arg).then(function (value) {
	            invoke("next", value, resolve, reject);
	          }, function (err) {
	            invoke("throw", err, resolve, reject);
	          });
	        }

	        return _Promise.resolve(value).then(function (unwrapped) {
	          // When a yielded Promise is resolved, its final value becomes
	          // the .value of the Promise<{value,done}> result for the
	          // current iteration. If the Promise is rejected, however, the
	          // result for this iteration will be rejected with the same
	          // reason. Note that rejections of yielded Promises are not
	          // thrown back into the generator function, as is the case
	          // when an awaited Promise is rejected. This difference in
	          // behavior between yield and await is important, because it
	          // allows the consumer to decide what to do with the yielded
	          // rejection (swallow it and continue, manually .throw it back
	          // into the generator, abandon iteration, whatever). With
	          // await, by contrast, there is no opportunity to examine the
	          // rejection reason outside the generator function, so the
	          // only option is to throw it from the await expression, and
	          // let the generator function handle the exception.
	          result.value = unwrapped;
	          resolve(result);
	        }, reject);
	      }
	    }

	    if (typeof process === "object" && process.domain) {
	      invoke = process.domain.bind(invoke);
	    }

	    var previousPromise;

	    function enqueue(method, arg) {
	      function callInvokeWithMethodAndArg() {
	        return new _Promise(function (resolve, reject) {
	          invoke(method, arg, resolve, reject);
	        });
	      }

	      return previousPromise =
	      // If enqueue has been called before, then we want to wait until
	      // all previous Promises have been resolved before calling invoke,
	      // so that results are always delivered in the correct order. If
	      // enqueue has not been called before, then it is important to
	      // call invoke immediately, without waiting on a callback to fire,
	      // so that the async generator function has the opportunity to do
	      // any necessary setup in a predictable way. This predictability
	      // is why the Promise constructor synchronously invokes its
	      // executor callback, and why async functions synchronously
	      // execute code before the first await. Since we implement simple
	      // async functions in terms of async generators, it is especially
	      // important to get this right, even though it requires care.
	      previousPromise ? previousPromise.then(callInvokeWithMethodAndArg,
	      // Avoid propagating failures to Promises returned by later
	      // invocations of the iterator.
	      callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg();
	    }

	    // Define the unified helper method that is used to implement .next,
	    // .throw, and .return (see defineIteratorMethods).
	    this._invoke = enqueue;
	  }

	  defineIteratorMethods(AsyncIterator.prototype);

	  // Note that simple async functions are implemented on top of
	  // AsyncIterator objects; they just return a Promise for the value of
	  // the final result produced by the iterator.
	  runtime.async = function (innerFn, outerFn, self, tryLocsList) {
	    var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList));

	    return runtime.isGeneratorFunction(outerFn) ? iter // If outerFn is a generator, return the full iterator.
	    : iter.next().then(function (result) {
	      return result.done ? result.value : iter.next();
	    });
	  };

	  function makeInvokeMethod(innerFn, self, context) {
	    var state = GenStateSuspendedStart;

	    return function invoke(method, arg) {
	      if (state === GenStateExecuting) {
	        throw new Error("Generator is already running");
	      }

	      if (state === GenStateCompleted) {
	        if (method === "throw") {
	          throw arg;
	        }

	        // Be forgiving, per 25.3.3.3.3 of the spec:
	        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
	        return doneResult();
	      }

	      while (true) {
	        var delegate = context.delegate;
	        if (delegate) {
	          if (method === "return" || method === "throw" && delegate.iterator[method] === undefined) {
	            // A return or throw (when the delegate iterator has no throw
	            // method) always terminates the yield* loop.
	            context.delegate = null;

	            // If the delegate iterator has a return method, give it a
	            // chance to clean up.
	            var returnMethod = delegate.iterator["return"];
	            if (returnMethod) {
	              var record = tryCatch(returnMethod, delegate.iterator, arg);
	              if (record.type === "throw") {
	                // If the return method threw an exception, let that
	                // exception prevail over the original return or throw.
	                method = "throw";
	                arg = record.arg;
	                continue;
	              }
	            }

	            if (method === "return") {
	              // Continue with the outer return, now that the delegate
	              // iterator has been terminated.
	              continue;
	            }
	          }

	          var record = tryCatch(delegate.iterator[method], delegate.iterator, arg);

	          if (record.type === "throw") {
	            context.delegate = null;

	            // Like returning generator.throw(uncaught), but without the
	            // overhead of an extra function call.
	            method = "throw";
	            arg = record.arg;
	            continue;
	          }

	          // Delegate generator ran and handled its own exceptions so
	          // regardless of what the method was, we continue as if it is
	          // "next" with an undefined arg.
	          method = "next";
	          arg = undefined;

	          var info = record.arg;
	          if (info.done) {
	            context[delegate.resultName] = info.value;
	            context.next = delegate.nextLoc;
	          } else {
	            state = GenStateSuspendedYield;
	            return info;
	          }

	          context.delegate = null;
	        }

	        if (method === "next") {
	          if (state === GenStateSuspendedYield) {
	            context.sent = arg;
	          } else {
	            context.sent = undefined;
	          }
	        } else if (method === "throw") {
	          if (state === GenStateSuspendedStart) {
	            state = GenStateCompleted;
	            throw arg;
	          }

	          if (context.dispatchException(arg)) {
	            // If the dispatched exception was caught by a catch block,
	            // then let that catch block handle the exception normally.
	            method = "next";
	            arg = undefined;
	          }
	        } else if (method === "return") {
	          context.abrupt("return", arg);
	        }

	        state = GenStateExecuting;

	        var record = tryCatch(innerFn, self, context);
	        if (record.type === "normal") {
	          // If an exception is thrown from innerFn, we leave state ===
	          // GenStateExecuting and loop back for another invocation.
	          state = context.done ? GenStateCompleted : GenStateSuspendedYield;

	          var info = {
	            value: record.arg,
	            done: context.done
	          };

	          if (record.arg === ContinueSentinel) {
	            if (context.delegate && method === "next") {
	              // Deliberately forget the last sent value so that we don't
	              // accidentally pass it on to the delegate.
	              arg = undefined;
	            }
	          } else {
	            return info;
	          }
	        } else if (record.type === "throw") {
	          state = GenStateCompleted;
	          // Dispatch the exception by looping back around to the
	          // context.dispatchException(arg) call above.
	          method = "throw";
	          arg = record.arg;
	        }
	      }
	    };
	  }

	  // Define Generator.prototype.{next,throw,return} in terms of the
	  // unified ._invoke helper method.
	  defineIteratorMethods(Gp);

	  Gp[iteratorSymbol] = function () {
	    return this;
	  };

	  Gp[toStringTagSymbol] = "Generator";

	  Gp.toString = function () {
	    return "[object Generator]";
	  };

	  function pushTryEntry(locs) {
	    var entry = { tryLoc: locs[0] };

	    if (1 in locs) {
	      entry.catchLoc = locs[1];
	    }

	    if (2 in locs) {
	      entry.finallyLoc = locs[2];
	      entry.afterLoc = locs[3];
	    }

	    this.tryEntries.push(entry);
	  }

	  function resetTryEntry(entry) {
	    var record = entry.completion || {};
	    record.type = "normal";
	    delete record.arg;
	    entry.completion = record;
	  }

	  function Context(tryLocsList) {
	    // The root entry object (effectively a try statement without a catch
	    // or a finally block) gives us a place to store values thrown from
	    // locations where there is no enclosing try statement.
	    this.tryEntries = [{ tryLoc: "root" }];
	    tryLocsList.forEach(pushTryEntry, this);
	    this.reset(true);
	  }

	  runtime.keys = function (object) {
	    var keys = [];
	    for (var key in object) {
	      keys.push(key);
	    }
	    keys.reverse();

	    // Rather than returning an object with a next method, we keep
	    // things simple and return the next function itself.
	    return function next() {
	      while (keys.length) {
	        var key = keys.pop();
	        if (key in object) {
	          next.value = key;
	          next.done = false;
	          return next;
	        }
	      }

	      // To avoid creating an additional object, we just hang the .value
	      // and .done properties off the next function object itself. This
	      // also ensures that the minifier will not anonymize the function.
	      next.done = true;
	      return next;
	    };
	  };

	  function values(iterable) {
	    if (iterable) {
	      var iteratorMethod = iterable[iteratorSymbol];
	      if (iteratorMethod) {
	        return iteratorMethod.call(iterable);
	      }

	      if (typeof iterable.next === "function") {
	        return iterable;
	      }

	      if (!isNaN(iterable.length)) {
	        var i = -1,
	            next = function next() {
	          while (++i < iterable.length) {
	            if (hasOwn.call(iterable, i)) {
	              next.value = iterable[i];
	              next.done = false;
	              return next;
	            }
	          }

	          next.value = undefined;
	          next.done = true;

	          return next;
	        };

	        return next.next = next;
	      }
	    }

	    // Return an iterator with no values.
	    return { next: doneResult };
	  }
	  runtime.values = values;

	  function doneResult() {
	    return { value: undefined, done: true };
	  }

	  Context.prototype = {
	    constructor: Context,

	    reset: function reset(skipTempReset) {
	      this.prev = 0;
	      this.next = 0;
	      this.sent = undefined;
	      this.done = false;
	      this.delegate = null;

	      this.tryEntries.forEach(resetTryEntry);

	      if (!skipTempReset) {
	        for (var name in this) {
	          // Not sure about the optimal order of these conditions:
	          if (name.charAt(0) === "t" && hasOwn.call(this, name) && !isNaN(+name.slice(1))) {
	            this[name] = undefined;
	          }
	        }
	      }
	    },

	    stop: function stop() {
	      this.done = true;

	      var rootEntry = this.tryEntries[0];
	      var rootRecord = rootEntry.completion;
	      if (rootRecord.type === "throw") {
	        throw rootRecord.arg;
	      }

	      return this.rval;
	    },

	    dispatchException: function dispatchException(exception) {
	      if (this.done) {
	        throw exception;
	      }

	      var context = this;
	      function handle(loc, caught) {
	        record.type = "throw";
	        record.arg = exception;
	        context.next = loc;
	        return !!caught;
	      }

	      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
	        var entry = this.tryEntries[i];
	        var record = entry.completion;

	        if (entry.tryLoc === "root") {
	          // Exception thrown outside of any try block that could handle
	          // it, so set the completion value of the entire function to
	          // throw the exception.
	          return handle("end");
	        }

	        if (entry.tryLoc <= this.prev) {
	          var hasCatch = hasOwn.call(entry, "catchLoc");
	          var hasFinally = hasOwn.call(entry, "finallyLoc");

	          if (hasCatch && hasFinally) {
	            if (this.prev < entry.catchLoc) {
	              return handle(entry.catchLoc, true);
	            } else if (this.prev < entry.finallyLoc) {
	              return handle(entry.finallyLoc);
	            }
	          } else if (hasCatch) {
	            if (this.prev < entry.catchLoc) {
	              return handle(entry.catchLoc, true);
	            }
	          } else if (hasFinally) {
	            if (this.prev < entry.finallyLoc) {
	              return handle(entry.finallyLoc);
	            }
	          } else {
	            throw new Error("try statement without catch or finally");
	          }
	        }
	      }
	    },

	    abrupt: function abrupt(type, arg) {
	      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
	        var entry = this.tryEntries[i];
	        if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) {
	          var finallyEntry = entry;
	          break;
	        }
	      }

	      if (finallyEntry && (type === "break" || type === "continue") && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc) {
	        // Ignore the finally entry if control is not jumping to a
	        // location outside the try/catch block.
	        finallyEntry = null;
	      }

	      var record = finallyEntry ? finallyEntry.completion : {};
	      record.type = type;
	      record.arg = arg;

	      if (finallyEntry) {
	        this.next = finallyEntry.finallyLoc;
	      } else {
	        this.complete(record);
	      }

	      return ContinueSentinel;
	    },

	    complete: function complete(record, afterLoc) {
	      if (record.type === "throw") {
	        throw record.arg;
	      }

	      if (record.type === "break" || record.type === "continue") {
	        this.next = record.arg;
	      } else if (record.type === "return") {
	        this.rval = record.arg;
	        this.next = "end";
	      } else if (record.type === "normal" && afterLoc) {
	        this.next = afterLoc;
	      }
	    },

	    finish: function finish(finallyLoc) {
	      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
	        var entry = this.tryEntries[i];
	        if (entry.finallyLoc === finallyLoc) {
	          this.complete(entry.completion, entry.afterLoc);
	          resetTryEntry(entry);
	          return ContinueSentinel;
	        }
	      }
	    },

	    "catch": function _catch(tryLoc) {
	      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
	        var entry = this.tryEntries[i];
	        if (entry.tryLoc === tryLoc) {
	          var record = entry.completion;
	          if (record.type === "throw") {
	            var thrown = record.arg;
	            resetTryEntry(entry);
	          }
	          return thrown;
	        }
	      }

	      // The context.catch method must only be called with a location
	      // argument that corresponds to a known catch block.
	      throw new Error("illegal catch attempt");
	    },

	    delegateYield: function delegateYield(iterable, resultName, nextLoc) {
	      this.delegate = {
	        iterator: values(iterable),
	        resultName: resultName,
	        nextLoc: nextLoc
	      };

	      return ContinueSentinel;
	    }
	  };
	})(
	// Among the various tricks for obtaining a reference to the global
	// object, this seems to be the most reliable technique that does not
	// use indirect eval (which violates Content Security Policy).
	typeof global === "object" ? global : typeof window === "object" ? window : typeof self === "object" ? self : undefined);
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }()), __webpack_require__(69)))

/***/ },
/* 69 */
/***/ function(module, exports) {

	// shim for using process in browser

	var process = module.exports = {};
	var queue = [];
	var draining = false;
	var currentQueue;
	var queueIndex = -1;

	function cleanUpNextTick() {
	    draining = false;
	    if (currentQueue.length) {
	        queue = currentQueue.concat(queue);
	    } else {
	        queueIndex = -1;
	    }
	    if (queue.length) {
	        drainQueue();
	    }
	}

	function drainQueue() {
	    if (draining) {
	        return;
	    }
	    var timeout = setTimeout(cleanUpNextTick);
	    draining = true;

	    var len = queue.length;
	    while(len) {
	        currentQueue = queue;
	        queue = [];
	        while (++queueIndex < len) {
	            if (currentQueue) {
	                currentQueue[queueIndex].run();
	            }
	        }
	        queueIndex = -1;
	        len = queue.length;
	    }
	    currentQueue = null;
	    draining = false;
	    clearTimeout(timeout);
	}

	process.nextTick = function (fun) {
	    var args = new Array(arguments.length - 1);
	    if (arguments.length > 1) {
	        for (var i = 1; i < arguments.length; i++) {
	            args[i - 1] = arguments[i];
	        }
	    }
	    queue.push(new Item(fun, args));
	    if (queue.length === 1 && !draining) {
	        setTimeout(drainQueue, 0);
	    }
	};

	// v8 likes predictible objects
	function Item(fun, array) {
	    this.fun = fun;
	    this.array = array;
	}
	Item.prototype.run = function () {
	    this.fun.apply(null, this.array);
	};
	process.title = 'browser';
	process.browser = true;
	process.env = {};
	process.argv = [];
	process.version = ''; // empty string to avoid regexp issues
	process.versions = {};

	function noop() {}

	process.on = noop;
	process.addListener = noop;
	process.once = noop;
	process.off = noop;
	process.removeListener = noop;
	process.removeAllListeners = noop;
	process.emit = noop;

	process.binding = function (name) {
	    throw new Error('process.binding is not supported');
	};

	process.cwd = function () { return '/' };
	process.chdir = function (dir) {
	    throw new Error('process.chdir is not supported');
	};
	process.umask = function() { return 0; };


/***/ },
/* 70 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = { "default": __webpack_require__(71), __esModule: true };

/***/ },
/* 71 */
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(59);
	__webpack_require__(31);
	__webpack_require__(3);
	__webpack_require__(72);
	module.exports = __webpack_require__(16).Promise;

/***/ },
/* 72 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	var $          = __webpack_require__(21)
	  , LIBRARY    = __webpack_require__(13)
	  , global     = __webpack_require__(15)
	  , ctx        = __webpack_require__(17)
	  , classof    = __webpack_require__(38)
	  , $export    = __webpack_require__(14)
	  , isObject   = __webpack_require__(36)
	  , anObject   = __webpack_require__(35)
	  , aFunction  = __webpack_require__(18)
	  , strictNew  = __webpack_require__(73)
	  , forOf      = __webpack_require__(74)
	  , setProto   = __webpack_require__(66).set
	  , same       = __webpack_require__(78)
	  , SPECIES    = __webpack_require__(28)('species')
	  , speciesConstructor = __webpack_require__(79)
	  , asap       = __webpack_require__(80)
	  , PROMISE    = 'Promise'
	  , process    = global.process
	  , isNode     = classof(process) == 'process'
	  , P          = global[PROMISE]
	  , Wrapper;

	var testResolve = function(sub){
	  var test = new P(function(){});
	  if(sub)test.constructor = Object;
	  return P.resolve(test) === test;
	};

	var USE_NATIVE = function(){
	  var works = false;
	  function P2(x){
	    var self = new P(x);
	    setProto(self, P2.prototype);
	    return self;
	  }
	  try {
	    works = P && P.resolve && testResolve();
	    setProto(P2, P);
	    P2.prototype = $.create(P.prototype, {constructor: {value: P2}});
	    // actual Firefox has broken subclass support, test that
	    if(!(P2.resolve(5).then(function(){}) instanceof P2)){
	      works = false;
	    }
	    // actual V8 bug, https://code.google.com/p/v8/issues/detail?id=4162
	    if(works && __webpack_require__(23)){
	      var thenableThenGotten = false;
	      P.resolve($.setDesc({}, 'then', {
	        get: function(){ thenableThenGotten = true; }
	      }));
	      works = thenableThenGotten;
	    }
	  } catch(e){ works = false; }
	  return works;
	}();

	// helpers
	var sameConstructor = function(a, b){
	  // library wrapper special case
	  if(LIBRARY && a === P && b === Wrapper)return true;
	  return same(a, b);
	};
	var getConstructor = function(C){
	  var S = anObject(C)[SPECIES];
	  return S != undefined ? S : C;
	};
	var isThenable = function(it){
	  var then;
	  return isObject(it) && typeof (then = it.then) == 'function' ? then : false;
	};
	var PromiseCapability = function(C){
	  var resolve, reject;
	  this.promise = new C(function($$resolve, $$reject){
	    if(resolve !== undefined || reject !== undefined)throw TypeError('Bad Promise constructor');
	    resolve = $$resolve;
	    reject  = $$reject;
	  });
	  this.resolve = aFunction(resolve),
	  this.reject  = aFunction(reject)
	};
	var perform = function(exec){
	  try {
	    exec();
	  } catch(e){
	    return {error: e};
	  }
	};
	var notify = function(record, isReject){
	  if(record.n)return;
	  record.n = true;
	  var chain = record.c;
	  asap(function(){
	    var value = record.v
	      , ok    = record.s == 1
	      , i     = 0;
	    var run = function(reaction){
	      var handler = ok ? reaction.ok : reaction.fail
	        , resolve = reaction.resolve
	        , reject  = reaction.reject
	        , result, then;
	      try {
	        if(handler){
	          if(!ok)record.h = true;
	          result = handler === true ? value : handler(value);
	          if(result === reaction.promise){
	            reject(TypeError('Promise-chain cycle'));
	          } else if(then = isThenable(result)){
	            then.call(result, resolve, reject);
	          } else resolve(result);
	        } else reject(value);
	      } catch(e){
	        reject(e);
	      }
	    };
	    while(chain.length > i)run(chain[i++]); // variable length - can't use forEach
	    chain.length = 0;
	    record.n = false;
	    if(isReject)setTimeout(function(){
	      var promise = record.p
	        , handler, console;
	      if(isUnhandled(promise)){
	        if(isNode){
	          process.emit('unhandledRejection', value, promise);
	        } else if(handler = global.onunhandledrejection){
	          handler({promise: promise, reason: value});
	        } else if((console = global.console) && console.error){
	          console.error('Unhandled promise rejection', value);
	        }
	      } record.a = undefined;
	    }, 1);
	  });
	};
	var isUnhandled = function(promise){
	  var record = promise._d
	    , chain  = record.a || record.c
	    , i      = 0
	    , reaction;
	  if(record.h)return false;
	  while(chain.length > i){
	    reaction = chain[i++];
	    if(reaction.fail || !isUnhandled(reaction.promise))return false;
	  } return true;
	};
	var $reject = function(value){
	  var record = this;
	  if(record.d)return;
	  record.d = true;
	  record = record.r || record; // unwrap
	  record.v = value;
	  record.s = 2;
	  record.a = record.c.slice();
	  notify(record, true);
	};
	var $resolve = function(value){
	  var record = this
	    , then;
	  if(record.d)return;
	  record.d = true;
	  record = record.r || record; // unwrap
	  try {
	    if(record.p === value)throw TypeError("Promise can't be resolved itself");
	    if(then = isThenable(value)){
	      asap(function(){
	        var wrapper = {r: record, d: false}; // wrap
	        try {
	          then.call(value, ctx($resolve, wrapper, 1), ctx($reject, wrapper, 1));
	        } catch(e){
	          $reject.call(wrapper, e);
	        }
	      });
	    } else {
	      record.v = value;
	      record.s = 1;
	      notify(record, false);
	    }
	  } catch(e){
	    $reject.call({r: record, d: false}, e); // wrap
	  }
	};

	// constructor polyfill
	if(!USE_NATIVE){
	  // 25.4.3.1 Promise(executor)
	  P = function Promise(executor){
	    aFunction(executor);
	    var record = this._d = {
	      p: strictNew(this, P, PROMISE),         // <- promise
	      c: [],                                  // <- awaiting reactions
	      a: undefined,                           // <- checked in isUnhandled reactions
	      s: 0,                                   // <- state
	      d: false,                               // <- done
	      v: undefined,                           // <- value
	      h: false,                               // <- handled rejection
	      n: false                                // <- notify
	    };
	    try {
	      executor(ctx($resolve, record, 1), ctx($reject, record, 1));
	    } catch(err){
	      $reject.call(record, err);
	    }
	  };
	  __webpack_require__(85)(P.prototype, {
	    // 25.4.5.3 Promise.prototype.then(onFulfilled, onRejected)
	    then: function then(onFulfilled, onRejected){
	      var reaction = new PromiseCapability(speciesConstructor(this, P))
	        , promise  = reaction.promise
	        , record   = this._d;
	      reaction.ok   = typeof onFulfilled == 'function' ? onFulfilled : true;
	      reaction.fail = typeof onRejected == 'function' && onRejected;
	      record.c.push(reaction);
	      if(record.a)record.a.push(reaction);
	      if(record.s)notify(record, false);
	      return promise;
	    },
	    // 25.4.5.1 Promise.prototype.catch(onRejected)
	    'catch': function(onRejected){
	      return this.then(undefined, onRejected);
	    }
	  });
	}

	$export($export.G + $export.W + $export.F * !USE_NATIVE, {Promise: P});
	__webpack_require__(27)(P, PROMISE);
	__webpack_require__(86)(PROMISE);
	Wrapper = __webpack_require__(16)[PROMISE];

	// statics
	$export($export.S + $export.F * !USE_NATIVE, PROMISE, {
	  // 25.4.4.5 Promise.reject(r)
	  reject: function reject(r){
	    var capability = new PromiseCapability(this)
	      , $$reject   = capability.reject;
	    $$reject(r);
	    return capability.promise;
	  }
	});
	$export($export.S + $export.F * (!USE_NATIVE || testResolve(true)), PROMISE, {
	  // 25.4.4.6 Promise.resolve(x)
	  resolve: function resolve(x){
	    // instanceof instead of internal slot check because we should fix it without replacement native Promise core
	    if(x instanceof P && sameConstructor(x.constructor, this))return x;
	    var capability = new PromiseCapability(this)
	      , $$resolve  = capability.resolve;
	    $$resolve(x);
	    return capability.promise;
	  }
	});
	$export($export.S + $export.F * !(USE_NATIVE && __webpack_require__(87)(function(iter){
	  P.all(iter)['catch'](function(){});
	})), PROMISE, {
	  // 25.4.4.1 Promise.all(iterable)
	  all: function all(iterable){
	    var C          = getConstructor(this)
	      , capability = new PromiseCapability(C)
	      , resolve    = capability.resolve
	      , reject     = capability.reject
	      , values     = [];
	    var abrupt = perform(function(){
	      forOf(iterable, false, values.push, values);
	      var remaining = values.length
	        , results   = Array(remaining);
	      if(remaining)$.each.call(values, function(promise, index){
	        var alreadyCalled = false;
	        C.resolve(promise).then(function(value){
	          if(alreadyCalled)return;
	          alreadyCalled = true;
	          results[index] = value;
	          --remaining || resolve(results);
	        }, reject);
	      });
	      else resolve(results);
	    });
	    if(abrupt)reject(abrupt.error);
	    return capability.promise;
	  },
	  // 25.4.4.4 Promise.race(iterable)
	  race: function race(iterable){
	    var C          = getConstructor(this)
	      , capability = new PromiseCapability(C)
	      , reject     = capability.reject;
	    var abrupt = perform(function(){
	      forOf(iterable, false, function(promise){
	        C.resolve(promise).then(capability.resolve, reject);
	      });
	    });
	    if(abrupt)reject(abrupt.error);
	    return capability.promise;
	  }
	});

/***/ },
/* 73 */
/***/ function(module, exports) {

	module.exports = function(it, Constructor, name){
	  if(!(it instanceof Constructor))throw TypeError(name + ": use the 'new' operator!");
	  return it;
	};

/***/ },
/* 74 */
/***/ function(module, exports, __webpack_require__) {

	var ctx         = __webpack_require__(17)
	  , call        = __webpack_require__(75)
	  , isArrayIter = __webpack_require__(76)
	  , anObject    = __webpack_require__(35)
	  , toLength    = __webpack_require__(77)
	  , getIterFn   = __webpack_require__(37);
	module.exports = function(iterable, entries, fn, that){
	  var iterFn = getIterFn(iterable)
	    , f      = ctx(fn, that, entries ? 2 : 1)
	    , index  = 0
	    , length, step, iterator;
	  if(typeof iterFn != 'function')throw TypeError(iterable + ' is not iterable!');
	  // fast case for arrays with default iterator
	  if(isArrayIter(iterFn))for(length = toLength(iterable.length); length > index; index++){
	    entries ? f(anObject(step = iterable[index])[0], step[1]) : f(iterable[index]);
	  } else for(iterator = iterFn.call(iterable); !(step = iterator.next()).done; ){
	    call(iterator, f, step.value, entries);
	  }
	};

/***/ },
/* 75 */
/***/ function(module, exports, __webpack_require__) {

	// call something on iterator step with safe closing on error
	var anObject = __webpack_require__(35);
	module.exports = function(iterator, fn, value, entries){
	  try {
	    return entries ? fn(anObject(value)[0], value[1]) : fn(value);
	  // 7.4.6 IteratorClose(iterator, completion)
	  } catch(e){
	    var ret = iterator['return'];
	    if(ret !== undefined)anObject(ret.call(iterator));
	    throw e;
	  }
	};

/***/ },
/* 76 */
/***/ function(module, exports, __webpack_require__) {

	// check on default Array iterator
	var Iterators  = __webpack_require__(7)
	  , ITERATOR   = __webpack_require__(28)('iterator')
	  , ArrayProto = Array.prototype;

	module.exports = function(it){
	  return it !== undefined && (Iterators.Array === it || ArrayProto[ITERATOR] === it);
	};

/***/ },
/* 77 */
/***/ function(module, exports, __webpack_require__) {

	// 7.1.15 ToLength
	var toInteger = __webpack_require__(33)
	  , min       = Math.min;
	module.exports = function(it){
	  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
	};

/***/ },
/* 78 */
/***/ function(module, exports) {

	// 7.2.9 SameValue(x, y)
	module.exports = Object.is || function is(x, y){
	  return x === y ? x !== 0 || 1 / x === 1 / y : x != x && y != y;
	};

/***/ },
/* 79 */
/***/ function(module, exports, __webpack_require__) {

	// 7.3.20 SpeciesConstructor(O, defaultConstructor)
	var anObject  = __webpack_require__(35)
	  , aFunction = __webpack_require__(18)
	  , SPECIES   = __webpack_require__(28)('species');
	module.exports = function(O, D){
	  var C = anObject(O).constructor, S;
	  return C === undefined || (S = anObject(C)[SPECIES]) == undefined ? D : aFunction(S);
	};

/***/ },
/* 80 */
/***/ function(module, exports, __webpack_require__) {

	var global    = __webpack_require__(15)
	  , macrotask = __webpack_require__(81).set
	  , Observer  = global.MutationObserver || global.WebKitMutationObserver
	  , process   = global.process
	  , Promise   = global.Promise
	  , isNode    = __webpack_require__(10)(process) == 'process'
	  , head, last, notify;

	var flush = function(){
	  var parent, domain, fn;
	  if(isNode && (parent = process.domain)){
	    process.domain = null;
	    parent.exit();
	  }
	  while(head){
	    domain = head.domain;
	    fn     = head.fn;
	    if(domain)domain.enter();
	    fn(); // <- currently we use it only for Promise - try / catch not required
	    if(domain)domain.exit();
	    head = head.next;
	  } last = undefined;
	  if(parent)parent.enter();
	};

	// Node.js
	if(isNode){
	  notify = function(){
	    process.nextTick(flush);
	  };
	// browsers with MutationObserver
	} else if(Observer){
	  var toggle = 1
	    , node   = document.createTextNode('');
	  new Observer(flush).observe(node, {characterData: true}); // eslint-disable-line no-new
	  notify = function(){
	    node.data = toggle = -toggle;
	  };
	// environments with maybe non-completely correct, but existent Promise
	} else if(Promise && Promise.resolve){
	  notify = function(){
	    Promise.resolve().then(flush);
	  };
	// for other environments - macrotask based on:
	// - setImmediate
	// - MessageChannel
	// - window.postMessag
	// - onreadystatechange
	// - setTimeout
	} else {
	  notify = function(){
	    // strange IE + webpack dev server bug - use .call(global)
	    macrotask.call(global, flush);
	  };
	}

	module.exports = function asap(fn){
	  var task = {fn: fn, next: undefined, domain: isNode && process.domain};
	  if(last)last.next = task;
	  if(!head){
	    head = task;
	    notify();
	  } last = task;
	};

/***/ },
/* 81 */
/***/ function(module, exports, __webpack_require__) {

	var ctx                = __webpack_require__(17)
	  , invoke             = __webpack_require__(82)
	  , html               = __webpack_require__(83)
	  , cel                = __webpack_require__(84)
	  , global             = __webpack_require__(15)
	  , process            = global.process
	  , setTask            = global.setImmediate
	  , clearTask          = global.clearImmediate
	  , MessageChannel     = global.MessageChannel
	  , counter            = 0
	  , queue              = {}
	  , ONREADYSTATECHANGE = 'onreadystatechange'
	  , defer, channel, port;
	var run = function(){
	  var id = +this;
	  if(queue.hasOwnProperty(id)){
	    var fn = queue[id];
	    delete queue[id];
	    fn();
	  }
	};
	var listner = function(event){
	  run.call(event.data);
	};
	// Node.js 0.9+ & IE10+ has setImmediate, otherwise:
	if(!setTask || !clearTask){
	  setTask = function setImmediate(fn){
	    var args = [], i = 1;
	    while(arguments.length > i)args.push(arguments[i++]);
	    queue[++counter] = function(){
	      invoke(typeof fn == 'function' ? fn : Function(fn), args);
	    };
	    defer(counter);
	    return counter;
	  };
	  clearTask = function clearImmediate(id){
	    delete queue[id];
	  };
	  // Node.js 0.8-
	  if(__webpack_require__(10)(process) == 'process'){
	    defer = function(id){
	      process.nextTick(ctx(run, id, 1));
	    };
	  // Browsers with MessageChannel, includes WebWorkers
	  } else if(MessageChannel){
	    channel = new MessageChannel;
	    port    = channel.port2;
	    channel.port1.onmessage = listner;
	    defer = ctx(port.postMessage, port, 1);
	  // Browsers with postMessage, skip WebWorkers
	  // IE8 has postMessage, but it's sync & typeof its postMessage is 'object'
	  } else if(global.addEventListener && typeof postMessage == 'function' && !global.importScripts){
	    defer = function(id){
	      global.postMessage(id + '', '*');
	    };
	    global.addEventListener('message', listner, false);
	  // IE8-
	  } else if(ONREADYSTATECHANGE in cel('script')){
	    defer = function(id){
	      html.appendChild(cel('script'))[ONREADYSTATECHANGE] = function(){
	        html.removeChild(this);
	        run.call(id);
	      };
	    };
	  // Rest old browsers
	  } else {
	    defer = function(id){
	      setTimeout(ctx(run, id, 1), 0);
	    };
	  }
	}
	module.exports = {
	  set:   setTask,
	  clear: clearTask
	};

/***/ },
/* 82 */
/***/ function(module, exports) {

	// fast apply, http://jsperf.lnkit.com/fast-apply/5
	module.exports = function(fn, args, that){
	  var un = that === undefined;
	  switch(args.length){
	    case 0: return un ? fn()
	                      : fn.call(that);
	    case 1: return un ? fn(args[0])
	                      : fn.call(that, args[0]);
	    case 2: return un ? fn(args[0], args[1])
	                      : fn.call(that, args[0], args[1]);
	    case 3: return un ? fn(args[0], args[1], args[2])
	                      : fn.call(that, args[0], args[1], args[2]);
	    case 4: return un ? fn(args[0], args[1], args[2], args[3])
	                      : fn.call(that, args[0], args[1], args[2], args[3]);
	  } return              fn.apply(that, args);
	};

/***/ },
/* 83 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(15).document && document.documentElement;

/***/ },
/* 84 */
/***/ function(module, exports, __webpack_require__) {

	var isObject = __webpack_require__(36)
	  , document = __webpack_require__(15).document
	  // in old IE typeof document.createElement is 'object'
	  , is = isObject(document) && isObject(document.createElement);
	module.exports = function(it){
	  return is ? document.createElement(it) : {};
	};

/***/ },
/* 85 */
/***/ function(module, exports, __webpack_require__) {

	var redefine = __webpack_require__(19);
	module.exports = function(target, src){
	  for(var key in src)redefine(target, key, src[key]);
	  return target;
	};

/***/ },
/* 86 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	var core        = __webpack_require__(16)
	  , $           = __webpack_require__(21)
	  , DESCRIPTORS = __webpack_require__(23)
	  , SPECIES     = __webpack_require__(28)('species');

	module.exports = function(KEY){
	  var C = core[KEY];
	  if(DESCRIPTORS && C && !C[SPECIES])$.setDesc(C, SPECIES, {
	    configurable: true,
	    get: function(){ return this; }
	  });
	};

/***/ },
/* 87 */
/***/ function(module, exports, __webpack_require__) {

	var ITERATOR     = __webpack_require__(28)('iterator')
	  , SAFE_CLOSING = false;

	try {
	  var riter = [7][ITERATOR]();
	  riter['return'] = function(){ SAFE_CLOSING = true; };
	  Array.from(riter, function(){ throw 2; });
	} catch(e){ /* empty */ }

	module.exports = function(exec, skipClosing){
	  if(!skipClosing && !SAFE_CLOSING)return false;
	  var safe = false;
	  try {
	    var arr  = [7]
	      , iter = arr[ITERATOR]();
	    iter.next = function(){ safe = true; };
	    arr[ITERATOR] = function(){ return iter; };
	    exec(arr);
	  } catch(e){ /* empty */ }
	  return safe;
	};

/***/ },
/* 88 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = { "default": __webpack_require__(89), __esModule: true };

/***/ },
/* 89 */
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(31);
	__webpack_require__(3);
	module.exports = __webpack_require__(28)('iterator');

/***/ },
/* 90 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	/**
	 * Class representing a generic setting which can be persisted by {@link Storage}.
	 *
	 * @access public
	 */

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.BoolSetting = exports.IntSetting = exports.Setting = undefined;

	var _isInteger = __webpack_require__(91);

	var _isInteger2 = _interopRequireDefault(_isInteger);

	var _typeof2 = __webpack_require__(51);

	var _typeof3 = _interopRequireDefault(_typeof2);

	var _getPrototypeOf = __webpack_require__(45);

	var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

	var _possibleConstructorReturn2 = __webpack_require__(50);

	var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

	var _inherits2 = __webpack_require__(60);

	var _inherits3 = _interopRequireDefault(_inherits2);

	var _getIterator2 = __webpack_require__(1);

	var _getIterator3 = _interopRequireDefault(_getIterator2);

	var _classCallCheck2 = __webpack_require__(43);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(40);

	var _createClass3 = _interopRequireDefault(_createClass2);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var Setting = function () {
	  /**
	   * Hook to run after changing the setting.
	   * @typedef {function(newVal: *, oldVal: *, setting: Setting)} ChangeSettingHook
	   */

	  /**
	   * Function to validate a new value.
	   *
	   * The new value considered invalid if the validator returns false or throws, and valid
	   * if the validator returns true.
	   * @typedef {function(value: *): boolean} SettingValidator
	   */

	  /**
	   * Function to parse a string into a setting's value
	   * @typedef {function(input: string): *} SettingParser
	   */

	  /**
	   * Create a new setting.
	   *
	   * @param {Storage} storage - storage backend.
	   * @param {string} key - a unique key which identifies the setting.
	   * @param {*} init - initial value of the setting.
	   * @param {Object} [options={}] - additional options.
	   * @param {string} [options.label] - a string describing the purpose of the setting.
	   * @param {string} [options.unit] - a unit of measurement of the setting.
	   * @param {boolean} [options.persistent=true] - indicate whether the setting needs to be
	   * persisted by the storage.
	   */

	  function Setting(storage, key, init) {
	    var options = arguments.length <= 3 || arguments[3] === undefined ? {} : arguments[3];
	    (0, _classCallCheck3.default)(this, Setting);

	    this._storage = storage;
	    this._key = key;
	    this._init = this._value = init;
	    this._validators = [];
	    this._onChangeHooks = [];

	    this._label = options.label;
	    this._unit = options.unit;
	    this._persistent = typeof options.persistent === 'boolean' ? options.persistent : true;

	    this._storage.registerSetting(this);
	  }

	  /**
	   * A string describing the purpose of the setting.
	   *
	   * @type {string}
	   */


	  (0, _createClass3.default)(Setting, [{
	    key: 'validator',


	    /**
	     * Register a validator function.
	     *
	     * @param {SettingValidator} validator
	     */
	    value: function validator(_validator) {
	      this._validators.push(_validator);
	    }

	    /**
	     * Validate a new value.
	     *
	     * Run all registered validators on the new value.
	     * @param {*} value - a new value that needs to be validated
	     * @return {boolean} true if the value if valid, false otherwise
	     */

	  }, {
	    key: 'validate',
	    value: function validate(value) {
	      try {
	        var _iteratorNormalCompletion = true;
	        var _didIteratorError = false;
	        var _iteratorError = undefined;

	        try {
	          for (var _iterator = (0, _getIterator3.default)(this._validators), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	            var validator = _step.value;

	            if (!validator(value)) {
	              return false;
	            }
	          }
	        } catch (err) {
	          _didIteratorError = true;
	          _iteratorError = err;
	        } finally {
	          try {
	            if (!_iteratorNormalCompletion && _iterator.return) {
	              _iterator.return();
	            }
	          } finally {
	            if (_didIteratorError) {
	              throw _iteratorError;
	            }
	          }
	        }
	      } catch (_e) {
	        return false;
	      }
	      return true;
	    }

	    /**
	     * The current setting's value.
	     */

	  }, {
	    key: 'parse',


	    /**
	     * Parse a new value.
	     *
	     * @return {*} the parsed value or null, if the parsing failed
	     */
	    value: function parse(value) {
	      if (typeof this._settingParser === 'function') {
	        return this._settingParser(value);
	      }
	      return value;
	    }

	    /**
	     * Set the setting's parser.
	     *
	     * @param {SettingParser} parser
	     */

	  }, {
	    key: 'parser',
	    value: function parser(_parser) {
	      this._settingParser = _parser;
	    }

	    /**
	     * Load previously stored value from the storage.
	     *
	     * Do nothing if {@link Setting#persistent} is false.
	     */

	  }, {
	    key: 'load',
	    value: function load() {
	      if (!this.persistent) {
	        return;
	      }

	      var value = this._storage.load(this);
	      if (value === null) {
	        return;
	      }

	      this.value = this.parse(value);
	    }

	    /**
	     * Store the current value to the storage.
	     *
	     * Do nothing if {@link Setting#persistent} is false.
	     */

	  }, {
	    key: 'save',
	    value: function save() {
	      if (!this.persistent) {
	        return;
	      }

	      this._storage.store(this);
	    }

	    /**
	     * Attempt to change the setting's value.
	     *
	     * Runs all hooks previously registered via {@link Setting#onChange} when
	     * the new value is different from the old one.
	     *
	     * @param {string} value - a new value.
	     * @return {boolean} true on success, false otherwise.
	     */

	  }, {
	    key: 'change',
	    value: function change(value) {
	      var parsedValue = this.parse(value);

	      if (parsedValue !== null && this.validate(parsedValue)) {
	        var oldValue = this.value;
	        this._value = parsedValue;

	        if (parsedValue !== oldValue) {
	          var _iteratorNormalCompletion2 = true;
	          var _didIteratorError2 = false;
	          var _iteratorError2 = undefined;

	          try {
	            for (var _iterator2 = (0, _getIterator3.default)(this._onChangeHooks), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
	              var hook = _step2.value;

	              hook(parsedValue, oldValue, this);
	            }
	          } catch (err) {
	            _didIteratorError2 = true;
	            _iteratorError2 = err;
	          } finally {
	            try {
	              if (!_iteratorNormalCompletion2 && _iterator2.return) {
	                _iterator2.return();
	              }
	            } finally {
	              if (_didIteratorError2) {
	                throw _iteratorError2;
	              }
	            }
	          }
	        }

	        return true;
	      }

	      return false;
	    }

	    /**
	     * Add hook run after changing the setting's value.
	     *
	     * @param {ChangeSettingHook} hook
	     */

	  }, {
	    key: 'onChange',
	    value: function onChange(hook) {
	      this._onChangeHooks.push(hook);
	    }

	    /**
	     * Render the setting into a DOM element.
	     *
	     * @return {jQuery}
	     */

	  }, {
	    key: 'render',
	    value: function render() {
	      var row = $('<div class="row"></div>');

	      $('<label>', {
	        'for': this.key,
	        text: this.label
	      }).appendTo(row);

	      var group = $('<div>', {
	        'class': 'input-group'
	      }).appendTo(row);

	      $('<input>', {
	        type: 'text',
	        id: this.key,
	        'class': 'setting',
	        name: this.key,
	        value: this.value,
	        'data-setting-key': this.key
	      }).appendTo(group);

	      var setting = this;

	      $('<span>', {
	        'class': 'badge',
	        'data-unit': this.unit,
	        on: {
	          click: function click() {
	            var badge = $(this);
	            var elem = badge.closest('.input-group').find('.setting');

	            if (!elem.hasClass('invalid')) {
	              return;
	            }

	            elem.val(setting.value);
	            elem.removeClass('invalid');
	          }
	        }
	      }).appendTo(group);

	      return row;
	    }

	    /**
	     * Extract a value from a DOM element.
	     *
	     * @param {jQuery} elem
	     * @return {*}
	     */

	  }, {
	    key: 'getInputValue',
	    value: function getInputValue(elem) {
	      return elem.val();
	    }

	    /**
	     * Set an input value from the setting.
	     *
	     * @param {jQuery} elem
	     */

	  }, {
	    key: 'setInputValue',
	    value: function setInputValue(elem) {
	      elem.val(this.value);
	    }

	    /**
	     * Set validation status of the input.
	     *
	     * @param {jQuery} elem
	     * @param {boolean} status
	     */

	  }, {
	    key: 'setValid',
	    value: function setValid(elem, status) {
	      elem.toggleClass('invalid', !status);
	    }
	  }, {
	    key: 'label',
	    get: function get() {
	      return this._label || this.key;
	    }

	    /**
	     * Initial value of the setting.
	     *
	     * @type {*}
	     */

	  }, {
	    key: 'init',
	    get: function get() {
	      return this._init;
	    }

	    /**
	     * A unique key which identifies the setting in the backend storage.
	     *
	     * @type {string}
	     */

	  }, {
	    key: 'key',
	    get: function get() {
	      return this._key;
	    }

	    /**
	     * Indicate whether the setting needs to be persisted by the storage.
	     *
	     * @type {boolean}
	     */

	  }, {
	    key: 'persistent',
	    get: function get() {
	      return this._persistent;
	    }

	    /**
	     * An optional unit of measurement of the setting.
	     *
	     * @type {string}
	     */

	  }, {
	    key: 'unit',
	    get: function get() {
	      return this._unit || '';
	    }
	  }, {
	    key: 'value',
	    get: function get() {
	      return this._value;
	    }

	    /**
	     * Set a new setting's value.
	     *
	     * Do nothing, if the new value is invalid.
	     */
	    ,
	    set: function set(value) {
	      if (this.validate(value)) {
	        this._value = value;
	      }
	    }
	  }]);
	  return Setting;
	}();

	/**
	 * Class representing an integer setting.
	 */


	exports.Setting = Setting;

	var IntSetting = function (_Setting) {
	  (0, _inherits3.default)(IntSetting, _Setting);

	  /**
	   * Range bound. {@link Range} {@link Range.lo} {@link Range#lo}
	   *
	   * It's possible to specify different types of ranges.
	   * - empty ([]) - both **range.hi** and **range.lo** are specified and **range.lo** > **range.hi**;
	   * - degenerate (single value [**range.lo** = **range.hi**]) - both **range.hi** and **range.lo** are specified and
	   *     **range.lo** = **range.hi**;
	   * - proper closed ([**range.lo**, **range.lo** + 1, ..., **range.hi**]) - **range.lo** < **range.hi**;
	   * - left-unbounded ([-**inf**, **range.hi**]) - only **range.hi** is specified;
	   * - right-unbounded ([**range.lo**, +**inf**]) - only **range.lo** is specified;
	   * - unbounded ([-**inf**, +**inf**]) - either **range** is not specified or
	   *    both **range.lo** and **range.hi** are not specified.
	   *
	   * @typedef {Object} Range
	   * @property {number} [lo] - the left bound of the range
	   * @property {number} [hi] - the right bound of the range
	   */

	  /**
	   * Create a new integer setting.
	   *
	   * @override
	   * @param {Storage} storage - storage backend.
	   * @param {string} key - a unique key which identifies the setting.
	   * @param {number} [init=0] - initial value of the setting.
	   * @param {Object} [options={}] - additional options.
	   * @param {string} [options.label] - a string describing the purpose of the setting.
	   * @param {string} [options.unit] - a unit of measurement of the setting.
	   * @param {boolean} [options.persistent=true] - indicate whether the setting needs to be persisted
	   * @param {Range} [options.range] - an optional range bound
	   */

	  function IntSetting(storage, key, init) {
	    var options = arguments.length <= 3 || arguments[3] === undefined ? {} : arguments[3];
	    (0, _classCallCheck3.default)(this, IntSetting);

	    var _this = (0, _possibleConstructorReturn3.default)(this, (0, _getPrototypeOf2.default)(IntSetting).call(this, storage, key, init || 0, options));

	    var rangeValidator = function rangeValidator(value) {
	      if (!((0, _typeof3.default)(_this._range) === 'object' && _this._range !== null)) {
	        return true;
	      }

	      if (typeof _this._range.lo === 'number' && typeof _this._range.hi !== 'number') {
	        return value >= _this._range.lo;
	      }

	      if (typeof _this._range.hi === 'number' && typeof _this._range.lo !== 'number') {
	        return value <= _this._range.hi;
	      }

	      if (typeof _this._range.hi === 'number' && typeof _this._range.lo === 'number') {
	        return value >= _this._range.lo && value <= _this._range.hi;
	      }

	      if (typeof _this._range.hi !== 'number' && typeof _this._range.lo !== 'number') {
	        return true;
	      }

	      return false;
	    };

	    _this.range(options.range);

	    _this.validator(_isInteger2.default);
	    _this.validator(rangeValidator);

	    _this.parser(function (value) {
	      return (/^[\-\+]?\d+$/.test(value) ? Number(value) : null
	      );
	    });
	    return _this;
	  }

	  /**
	   * Set a range bound for the setting.
	   *
	   * @param {Range} range
	   */


	  (0, _createClass3.default)(IntSetting, [{
	    key: 'range',
	    value: function range(_range) {
	      this._range = _range;
	    }
	  }]);
	  return IntSetting;
	}(Setting);

	/**
	 * Class representing an on/off setting.
	 */


	exports.IntSetting = IntSetting;

	var BoolSetting = exports.BoolSetting = function (_Setting2) {
	  (0, _inherits3.default)(BoolSetting, _Setting2);

	  /**
	   * Create a new boolean setting.
	   *
	   * @override
	   * @param {Storage} storage - storage backend.
	   * @param {string} key - a unique key which identifies the setting.
	   * @param {boolean} [init=false] - initial value of the setting.
	   * @param {Object} [options={}] - additional options.
	   * @param {string} [options.label] - a string describing the purpose of the setting.
	   * @param {string} [options.unit] - a unit of measurement of the setting.
	   * @param {boolean} [options.persistent=true] - indicate whether the setting needs to be persisted
	   * by the storage
	   */

	  function BoolSetting(storage, key, init) {
	    var options = arguments.length <= 3 || arguments[3] === undefined ? {} : arguments[3];
	    (0, _classCallCheck3.default)(this, BoolSetting);

	    var _this2 = (0, _possibleConstructorReturn3.default)(this, (0, _getPrototypeOf2.default)(BoolSetting).call(this, storage, key, init || false, options));

	    _this2.parser(function (value) {
	      if (isBoolean(value)) {
	        return value;
	      }

	      var values = {
	        'true': true,
	        'false': false
	      };

	      var result = values[value];

	      if (result === undefined) {
	        result = null;
	      }

	      return result;
	    });

	    _this2.validator(isBoolean);

	    function isBoolean(value) {
	      return typeof value === 'boolean';
	    }
	    return _this2;
	  }

	  /**
	   * Render the setting into a DOM element.
	   *
	   * @override
	   * @return {jQuery}
	   */


	  (0, _createClass3.default)(BoolSetting, [{
	    key: 'render',
	    value: function render() {
	      var row = $('<div class="row"></div>');

	      $('<label>', {
	        'for': this.key,
	        text: this.label
	      }).appendTo(row);

	      var group = $('<div>', {
	        'class': 'input-group'
	      }).appendTo(row);

	      $('<input>', {
	        type: 'checkbox',
	        id: this.key,
	        'class': 'setting',
	        name: this.key,
	        checked: this.value,
	        'data-setting-key': this.key
	      }).appendTo(group);

	      $('<label>', {
	        'class': 'checkbox-overlay',
	        'for': this.key
	      }).appendTo(group);

	      return row;
	    }

	    /**
	     * Extract a value from a DOM element.
	     *
	     * @override
	     * @param {jQuery} elem
	     * @return {*}
	     */

	  }, {
	    key: 'getInputValue',
	    value: function getInputValue(elem) {
	      return elem.is(':checked');
	    }

	    /**
	     * Set an input value from the setting.
	     *
	     * @override
	     * @param {jQuery} elem
	     */

	  }, {
	    key: 'setInputValue',
	    value: function setInputValue(elem) {
	      elem.prop('checked', this.value);
	    }
	  }]);
	  return BoolSetting;
	}(Setting);

/***/ },
/* 91 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = { "default": __webpack_require__(92), __esModule: true };

/***/ },
/* 92 */
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(93);
	module.exports = __webpack_require__(16).Number.isInteger;

/***/ },
/* 93 */
/***/ function(module, exports, __webpack_require__) {

	// 20.1.2.3 Number.isInteger(number)
	var $export = __webpack_require__(14);

	$export($export.S, 'Number', {isInteger: __webpack_require__(94)});

/***/ },
/* 94 */
/***/ function(module, exports, __webpack_require__) {

	// 20.1.2.3 Number.isInteger(number)
	var isObject = __webpack_require__(36)
	  , floor    = Math.floor;
	module.exports = function isInteger(it){
	  return !isObject(it) && isFinite(it) && floor(it) === it;
	};

/***/ },
/* 95 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var FAVICON_NOTIFY = exports.FAVICON_NOTIFY = "data:image/x-icon;base64,AAABAAEAICAAAAEAIACoEAAAFgAAACgAAAAgAAAAQAAAAAEAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD///8BAAAAAAD//wEAAAAAAAAAAAAAAAAAAAAAQEAABBQAAA0AAAAVAAAAEQAAAAUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP//AAEzmZkFAAAACgAAABcAAAAzAAAAUQAAAFkDAABdAAwXQgAAABcAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADDAwMFgAAAC0OJSo3HlZtRAtahl0AAC1EAAAALwAAADUXS2dmCUZwkhhhi60xjbbSHWSQ1g5TgbwQOFFxAAAAPgkiMx4AAAADAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUPLj4hAAAAQx5df5kykLnvOZ3K/0KfwfdCn7q9I2iGkw80S3oITHmlLY258Ei95v8ogLHIAFqaWxtKXiYAAAAgDRsoEwAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAATNmZgUAAAASGF2AYCyGsadIq8mmYdHnwF3a+/86p9XTAAAAKAAAABMml8mbKavj2hqAvxQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAADKKjiyiKa3asAAAAAAAAAAByZ34AOmd7bAIDVDAAAAAAAgNUGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAWYfQAEx42wAAABgAAAADAEpvtwxhif8WOldGAAAAAABVgAYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWZmZgUAAAAAAAAARSYiFfsRBgD/AAAA0Ts4O8Nwamf4Ukc9/wAAAKIAAAAAAAAAAAAAAAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABVVVUDAAAAAAAAAA+SkZHHysfH/5yVlf2EeXr/l4uM/7itrf+km53/Qj9A+gAAAEwAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP///wMAAAAAkZGRffLy8v/18vL/yL/A/LSmpvuhk4z6fXBr/paMivp8dXX/AAAAwRISEg4AAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAgIAEAAAAAAAAACfS0tPk//////Hv8Pvm4+L/19HQ/3lws/8AAIz/ODNS/GVeWf8vKyf/AAAAVAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAICAgAQAAAAAAAAAeKKin//4+Pj//P3+/P/////V1OX/JRjd/wAA9f8AALT/Jh5e+yklJf8AAACNAAAAAAAAAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAEAAAAADUwijAAAHbcMzJn/93d2/z//////////6inwf8AALP/HQPr/wAA4P8PAKX6KSZt/xEPOaYAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAgAAAAAAAAACAACmrxAA9/8AALr9uLjD/v//////////1tbd/0E/h/8AAJL/JyGs/5mYz/pubJ//AAAcr///AAIAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABVVVUGAAAAADk5L0w7OKH9Myf//wAA1/qSkbH//v77////////////ysbU/3Rpiv+DfZ3/razO+1VUdv8AAADlS0tGLAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGZmmQUAAAAAZWVfK3h3sONCPNz/QDy9+tDQ2f/y8vD////////+/v/n4uH/zcS//9LL0P+TkqL+Njc4/yIiGv8AAABhAAAAAAAAAAIAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMAAAAANzGKYl9dkf/a2uv//////PHx8f/+/v////////Du7//t6uv/6efm/5CQjP1FRUT/KCgq/wAAAFAAAAAAAAAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAd3dikv//+////////v7+/f/////+/v7//Pz8///////T09T/SkpL+wAAAP8JCgnrKSkpLAAAAAAAAAAFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAbW1tBwAAAAAAAAA2vby98/v5+f/v7Oz76+jo//Hv7//08fH/6ufn/5yZmf8lJSX6DAsL/w8PD84hISEXAAAAAAAAAAUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAClRUFDl5OPj/+rn5/ve29v/9/b2//z7+//t6uv/i4mJ/wAAAPkRERH/HBwcmgAAAAAAAAAAAAAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEBAQAQAAAAAAAAALUBBQfPu7u7/////+fDx8f/9/f7///////b39v91dXX8AAAA/x4eHv8YGBhqAAAAAAAAAAIAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAACSkpIHAAAAsbi5uf///////v7+/fn4+P//////7+/v/WZmZv9RUVH/ZWVllwAAAA8AAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAASKCsvlNbW1/3//////Pv5/v////78/P3+hYWG/xgYGJ+ZmZn/mZmZ/5mZmf+ZmZn/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgICABAAAAAAAAAAAsaeitMDd9/+HtuD97vP4+f////+ZmZn/mZmZ/5mZmf8Y3An/GNwJ/5mZmf+ZmZn/mZmZ/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD//wEAAAAAJNv/B1LJ60tMgZ7PI4rD/wCNyP9ips35kai8/5mZmf8Y3An/GNwJ/xjcCf8Y3An/GNwJ/xjcCf+ZmZn/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP//AQAAAAAAAAAANKbWiiKn3f8modT/KaHX/gCMz/yZmZn/mZmZ/xjcCf8Y3An/GNwJ/xjcCf8Y3An/GNwJ/5mZmf+ZmZn/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQIC/BAAAAAByeI0xRo2+9B6a3v8xlNP9WJK//pmZmf8Y3An/GNwJ/xjcCf8Y3An/GNwJ/xjcCf8Y3An/GNwJ/5mZmf8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACqqqoDAAAAAKylmkexr678eJKp/2+Elf3DwLz/mZmZ/xjcCf8Y3An/GNwJ/xjcCf8Y3An/GNwJ/xjcCf8Y3An/mZmZ/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKqqqgMAAAAAZ2tvRYyLiv1oYVr/Qzsy/Hx8fPqZmZn/mZmZ/xjcCf8Y3An/GNwJ/xjcCf8Y3An/GNwJ/5mZmf+ZmZn/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwAAAAAAAAAZAAAA5C8yNP8wMjT8AAAA/QAAAP+ZmZn/GNwJ/xjcCf8Y3An/GNwJ/xjcCf8Y3An/mZmZ/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAA1NTVqNTU1/A8PD/8XFxb/ISEh3ZmZmf+ZmZn/mZmZ/xjcCf8Y3An/mZmZ/5mZmf+ZmZn/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAHBwdHHR0dhBgYGHYcHBwkAAAAAAAAAACZmZn/mZmZ/5mZmf+ZmZn/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/////////////g///wIf//+DP///8z////M////wH///4B///+AP///AD///wAf//4AH//8AB///AAf//wAH//+AB///gAf//8AH///AB///wA///8AP///gAf//8AB///AAf//gAD//8AA///AAP//wAD//8AB///gAf//94f8=";

	var FAVICON_ORIGINAL = exports.FAVICON_ORIGINAL = "data:image/x-icon;base64,AAABAAEAICAAAAEAIACoEAAAFgAAACgAAAAgAAAAQAAAAAEAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8B////AAD//wH///8A////AP///wD///8AQEAABBQAAA0AAAAVAAAAEQAAAAX///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP//AAEzmZkFAAAACgAAABcAAAAzAAAAUQAAAFkDAABdAAwXQgAAABcAAAAE////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wAAAAADDAwMFgAAAC0OJSo3HlZtRAtahl0AAC1EAAAALwAAADUXS2dmCUZwkhhhi60xjbbSHWSQ1g5TgbwQOFFxAAAAPgkiMx4AAAAD////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AAAAAAUPLj4hAAAAQx5df5kykLnvOZ3K/0KfwfdCn7q9I2iGkw80S3oITHmlLY258Ei95v8ogLHIAFqaWxtKXiYAAAAgDRsoEwAAAAL///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8AAAAAATNmZgUAAAASGF2AYCyGsadIq8mmYdHnwF3a+/86p9XTAAAAKAAAABMml8mbKavj2hqAvxT///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wAAAAAB////AP///wAAAAADKKjiyiKa3av///8A////AByZ34AOmd7bAIDVDP///wAAgNUG////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wAAAAAB////AP///wD///8A////AP///wAAWYfQAEx42wAAABgAAAADAEpvtwxhif8WOldG////AABVgAb///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8AAAAAAWZmZgX///8AAAAARSYiFfsRBgD/AAAA0Ts4O8Nwamf4Ukc9/wAAAKL///8A////AAAAAAP///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wBVVVUD////AAAAAA+SkZHHysfH/5yVlf2EeXr/l4uM/7itrf+km53/Qj9A+gAAAEz///8AAAAABP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wP///8AkZGRffLy8v/18vL/yL/A/LSmpvuhk4z6fXBr/paMivp8dXX/AAAAwRISEg7///8AAAAABP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wCAgIAE////AAAAACfS0tPk//////Hv8Pvm4+L/19HQ/3lws/8AAIz/ODNS/GVeWf8vKyf/AAAAVP///wAAAAAE////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AICAgAT///8AAAAAeKKin//4+Pj//P3+/P/////V1OX/JRjd/wAA9f8AALT/Jh5e+yklJf8AAACN////AP///wAAAAAC////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wAAAIAE////ADUwijAAAHbcMzJn/93d2/z//////////6inwf8AALP/HQPr/wAA4P8PAKX6KSZt/xEPOab///8A////AAAAAAT///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8AAACAAv///wAAAAACAACmrxAA9/8AALr9uLjD/v//////////1tbd/0E/h/8AAJL/JyGs/5mYz/pubJ//AAAcr///AAL///8AAAAABP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wBVVVUG////ADk5L0w7OKH9Myf//wAA1/qSkbH//v77////////////ysbU/3Rpiv+DfZ3/razO+1VUdv8AAADlS0tGLP///wAAAAAE////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AGZmmQX///8AZWVfK3h3sONCPNz/QDy9+tDQ2f/y8vD////////+/v/n4uH/zcS//9LL0P+TkqL+Njc4/yIiGv8AAABh////AAAAAAIAAAAB////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AAAAAAP///8ANzGKYl9dkf/a2uv//////PHx8f/+/v////////Du7//t6uv/6efm/5CQjP1FRUT/KCgq/wAAAFD///8AAAAAA////wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8AAAAAAv///wD///8Ad3dikv//+////////v7+/f/////+/v7//Pz8///////T09T/SkpL+wAAAP8JCgnrKSkpLP///wAAAAAF////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8AbW1tB////wAAAAA2vby98/v5+f/v7Oz76+jo//Hv7//08fH/6ufn/5yZmf8lJSX6DAsL/w8PD84hISEX////AAAAAAX///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wAAAAAE////AAAAAClRUFDl5OPj/+rn5/ve29v/9/b2//z7+//t6uv/i4mJ/wAAAPkRERH/HBwcmv///wD///8AAAAAA////wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AEBAQAT///8AAAAALUBBQfPu7u7/////+fDx8f/9/f7///////b39v91dXX8AAAA/x4eHv8YGBhq////AAAAAAIAAAAB////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8AAAAAAv///wCSkpIHAAAAsbi5uf///////v7+/fn4+P//////7+/v/WZmZv9RUVH/ZWVllwAAAA////8AAAAAAv///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8AAAAAAv///wAAAAASKCsvlNbW1/3//////Pv5/v////78/P3+hYWG/xgYGJ////8A////AKqqqgP///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8AgICABP///wD///8AsaeitMDd9/+HtuD97vP4+f////+Pj47vAAAANP///wAgICAI////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AAD//wH///8AJNv/B1LJ60tMgZ7PI4rD/wCNyP9ips35kai8/1hYWusAAAAz////AAAAAAb///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8AAP//Af///wD///8ANKbWiiKn3f8modT/KaHX/gCMz/wAa6v/ADNR/xoAAGL///8AAAAABP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8AQIC/BP///wByeI0xRo2+9B6a3v8xlNP9WJK//oSWp/4tMDT/AAAAjP///wD///8AAAAAAf///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wCqqqoD////AKylmkexr678eJKp/2+Elf3DwLz/xsPA/Dk3NP8AAACX////AP///wAAAAAC////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AKqqqgP///8AZ2tvRYyLiv1oYVr/Qzsy/Hx8fPp1dnf9AAAA/wQAAHb///8AAAAAA////wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8AAAAAA////wAAAAAZAAAA5C8yNP8wMjT8AAAA/QAAAP8VFRXdJiYmIv///wAzMzMF////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wAAAAAB////AP///wA1NTVqNTU1/A8PD/8XFxb/ISEh3SEhIUb///8AAAAAAv///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wAAAAAB////AP///wAHBwdHHR0dhBgYGHYcHBwk////AP///wAAAAAB////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A/////////////g///wIf//+DP///8z////M////wH///4B///+AP///AD///wAf//4AH//8AB///AAf//wAH//+AB///gAf//8AH///AB///wA///8AP///gH///8D////A////gP///8B////Af///wP///8D////h////9//8=";

/***/ }
/******/ ]);